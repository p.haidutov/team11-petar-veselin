**Virtual Wallet**

[![pipeline status](https://gitlab.com/gableroux/unity3d-gitlab-ci-example/badges/master/pipeline.svg)](https://gitlab.com/gableroux/unity3d-gitlab-ci-example/commits/master)
[![Build Status](https://travis-ci.com/GabLeRoux/unity3d-ci-example.svg?branch=master)](https://travis-ci.com/GabLeRoux/unity3d-ci-example)

Our hosted website [link](https://vp-virtual-wallet.herokuapp.com/).

**Description of the Virtual Wallet project:**

   Virtual Wallet is a web application that enables you to consistently manage your budget.
Every user can send and receive money (user to user) and put money in his virtual wallet (bank to app).
   - A user can login/logout, change their password, email, photo, add and
change their credit/debit card, make transfers to other users and view
the history of their transfers.
   - Admin users have full access to every user and functionality of the
app.
   - Technologies used: Java 8, MariaDB, JPA, Spring MVC (Thymeleaf),
Spring Security, HTML, CSS, JS.

**This project uses technology components as following:**
   - Java 8, MariaDB, JPA;
   - Spring Framework, HTML, CSS, JS, Swagger2 (UI), Jquery, Bootstrap;
    
**Documentation:**
    
 - Swagger Docs - [link](http://localhost:8080/v2/api-docs)
    
 - Swagger UI - [link](http://localhost:8080/swagger-ui.html#/)

**Steps to build and run the project:**

*Prerequisites*:
In order to build and run the project you must install all the related Frameworks and tools described above in tech components section!

1. Clone or download the repository from the upper right corner buttons.
2. Unzip the project in folder of your choice.
3. Open the project in your IDE.
4. Run provided SQL scripts in sql database folder of the project.
5. Build it.
6. Run it.
6. Enjoy!

Invitational [link](https://trello.com/invite/b/Qa0TqZv2/852c1923da9ed3947a6d5d5cd35048dd/virtual-wallet) to "Trello" board.

Some pictures of the app:

 - Home Page
 
![ScreenShot](screenshots/HomePage.png)

 - Send money Page
 
 ![ScreenShot](screenshots/SendMoney.png)
 
 
  - Sign up Page
  
  ![ScreenShot](screenshots/SignUp.png)
  
  - Dashboard
  
  ![ScreenShot](screenshots/Dashboard.png)

