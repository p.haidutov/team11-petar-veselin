DROP SCHEMA IF EXISTS `virtual_wallet`;
create database if not exists virtual_wallet;
use virtual_wallet;

create table users
(
    id                      int(10)     not null auto_increment,
    username                varchar(50) not null unique,
    password                varchar(68) not null,
    email                   varchar(50) not null unique,
    phone                   varchar(15) not null unique,
    enabled                 boolean     not null default true,
    blocked                 boolean     not null default false,
    first_name              varchar(30),
    last_name               varchar(30),
    picture                 longblob,
    primary key (username),
    index (id),
    check (length(username) > 3),
    check (length(password) > 5),
    check (length(email) > 7)
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    unique key Username_Authority (username, authority),
    constraint authorities_fk foreign key (username) references users (username)
);

create table users_friends
(
    id              int(10)     not null auto_increment,
    user_username   varchar(50) not null,
    friend_username varchar(50) not null,
    primary key (id),
    foreign key (user_username) references users (username),
    foreign key (friend_username) references users (username),
    constraint unique (user_username, friend_username)
);

create table cards
(
    id            int(10)     not null auto_increment,
    user_username varchar(50) not null,
    enabled       boolean     not null default true,
    number        varchar(19) not null unique,
    exp_date      datetime    not null,
    security_code varchar(3)  not null,
    owner_name    varchar(50) not null,
    primary key (id),
    foreign key (user_username) references users (username),
    check (length(number) > 15),
    check (length(security_code) > 2),
    check (length(owner_name) > 4)
);

create table wallets_types
(
    id   int(10)     not null auto_increment,
    type varchar(50) not null,
    primary key (id)
);

create table wallets
(
    id            int(10)        not null auto_increment,
    type_id       int(10)        not null default 1,
    name          varchar(50)    not null default 'Primary',
    user_username varchar(50)    not null,
    balance       decimal(10, 2) not null,
    enabled       boolean        not null default true,
    primary key (id),
    foreign key (user_username) references users (username),
    foreign key (type_id) references wallets_types (id)
);

create table categories
(
    id       int(10)      not null auto_increment,
    name     varchar(255) not null,
    username varchar(50)  not null,
    enabled  boolean      not null default true,
    primary key (id),
    foreign key (username) references users (username)
) AUTO_INCREMENT = 2;

create table transactions
(
    transaction_id  int(10)        not null auto_increment,
    date            datetime       not null default current_timestamp,
    from_username   varchar(50)    not null,
    to_username     varchar(50)    not null,
    from_wallet_id  int(10)        not null,
    to_wallet_id    int(10)        not null,
    amount          decimal(10, 2) not null,
    category_id     int(10),
    idempotency_key varchar(36),
    description     varchar(1000),
    currency        varchar(3),
    primary key (transaction_id),
    foreign key (from_username) references users (username),
    foreign key (to_username) references users (username),
    foreign key (category_id) references categories (id)
);

create table top_up
(
    id              int(10)        not null auto_increment,
    username        varchar(50)    not null,
    date            timestamp      not null default now() on update now(),
    card_id         int(10)        not null,
    wallet_id       int(10)        not null,
    amount          decimal(10, 2) not null,
    idempotency_key varchar(36),
    description     varchar(1000),
    currency        varchar(3),
    primary key (id),
    foreign key (username) references users(username),
    foreign key (card_id) references cards (id),
    foreign key (wallet_id) references wallets (id)
);

create table tokens
(
    token_id     bigint auto_increment
        primary key,
    token        varchar(255)                        null,
    created_date timestamp default CURRENT_TIMESTAMP null,
    username     varchar(50)                         null,
    enabled      int                                 null,
    constraint tokens_users_username_fk
        foreign key (username) references virtual_wallet.users (username)
);
