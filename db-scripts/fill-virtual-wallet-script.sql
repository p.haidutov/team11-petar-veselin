use virtual_wallet;

insert into users(username, password, email, phone)
values ('pesho', '123456', 'pesho@gmail.com', '+359888121517'),
       ('doni', '123456', 'doni@gmail.com', '+359888212527'),
       ('vesko', '123456', 'vesko@gmail.com', '+359888171512'),
       ('mitko', '123456', 'mitko@gmail.com', '+359888151515'),
       ('gosho', '123456', 'gosho@gmail.com', '+359888171717'),
       ('blocked', '123456', 'blocked@gmail.com', '+359888171700');

insert into wallets_types(type)
values ('primary'),
       ('secondary');

insert into wallets(user_username, name, balance)
values ('pesho', 'Pesho-wallet1', 250.20),
       ('doni', 'Doni-wallet1', 400.35),
       ('vesko', 'Vesko-wallet1', 231.38),
       ('gosho', 'Gosho-wallet1', 789.35),
       ('mitko', 'Mitko-wallet1', 215.35);

insert into cards(number, exp_date, security_code, owner_name, user_username)
values ('1234 5678 9123 4567', current_timestamp, 123, 'Pesho Peshov', 'pesho'),
       ('1234 5678 9123 4566', '2019-03-10 02:55:05', 234, 'Vesko Veskov', 'vesko'),
       ('1234 5678 9123 4577', '2019-03-10 02:55:05', 345, 'Mitko Mitkov', 'mitko'),
       ('1234 5678 9123 4588', '2019-03-10 02:55:05', 456, 'Gosho Goshov', 'gosho'),
       ('1234 5678 9123 4599', '2019-03-10 02:55:05', 567, 'Grisho Grishov', 'vesko');

insert into authorities(username, authority)
values ('pesho', 'ROLE_USER'),
       ('doni', 'ROLE_USER'),
       ('vesko', 'ROLE_USER'),
       ('mitko', 'ROLE_USER'),
       ('gosho', 'ROLE_USER'),
       ('pesho', 'ROLE_ADMIN'),
       ('vesko', 'ROLE_ADMIN'),
       ('blocked', 'ROLE_USER'),
       ('blocked', 'ROLE_ADMIN');

insert into categories(name, username)
values ('Rent', 'pesho'),
       ('Eating', 'pesho'),
       ('General', 'mitko'),
       ('General', 'doni'),
       ('Rent', 'vesko'),
       ('Utilities', 'vesko');