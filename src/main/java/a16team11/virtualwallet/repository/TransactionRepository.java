package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    Page<Transaction> getAllByOrderByDateDesc(Pageable pageable);

    List<Transaction> getTop5ByFromUserOrToUserOrderByDateDesc(String fromUsername, String toUsername);

    Transaction getByIdempotencyKey(String idempotencyKey);

    boolean existsByIdempotencyKey(String idempotencyKey);

    List<Transaction> getTransactionByFromUserAndToUserContainingAndCategoryAndDateBetween(String fromUsername,
                                                                                           String toUsername,
                                                                                           Category category,
                                                                                           Date start,
                                                                                           Date end);

    @Query("from Transaction where ((fromUser = ?1 or toUser = ?2) or toUser like ?3) and date between ?4 and ?5")
    List<Transaction> getTransactionByFromUserOrToUserAndToUserContainingAndDateBetween(String fromUsername,
                                                                                        String fromUsername2,
                                                                                        String toUsername,
                                                                                        Date start,
                                                                                        Date end);

    List<Transaction> getTransactionByFromUserAndToUserContainingAndDateBetween(String fromUsername,
                                                                                String toUsername,
                                                                                Date start,
                                                                                Date end);

    List<Transaction> getTransactionByFromUserContainingAndToUserContainingAndDateBetween(String fromUsername,
                                                                                          String toUsername,
                                                                                          Date start,
                                                                                          Date end);
}
