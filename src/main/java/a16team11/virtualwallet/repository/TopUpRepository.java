package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface TopUpRepository extends JpaRepository<TopUp, Integer> {

    List<TopUp> getAllByUserOrderByDateDesc(User user);

    List<TopUp> getAllByUserAndDateBetweenOrderByDate(User user, Date startDate, Date endDate);

    List<TopUp> getAllByUserOrUserAndDateBetweenOrderByDate(User user, User user2, Date startDate, Date endDate);

    List<TopUp> getAllByDateBetweenOrderByDate(Date startDate, Date endDate);

    List<TopUp> getTop5ByUserOrderByDateDesc(User user);

    TopUp getByIdempotencyKey(String idempotencyKey);

    boolean existsByIdempotencyKey(String idempotencyKey);
}
