package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.models.WalletType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WalletRepository extends JpaRepository<Wallet, Integer>{
    Wallet getWalletByIdAndEnabledTrue(int id);

    Wallet getWalletByNameAndEnabledTrue(String name);

    Wallet getWalletByUserAndWalletType(User user, WalletType walletType);

    boolean existsWalletByNameAndUser(String name, User user);

    List<Wallet> getAllByUserAndEnabledTrue(User user);
}
