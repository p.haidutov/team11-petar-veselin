package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface UserRepository extends JpaRepository<User, String> {

    List<User> findAllByEnabledTrue();

    User getUserByIdAndEnabledTrue(int id);

    User getUserByUsernameAndEnabledTrue(String username);

    User getUserByPhoneAndEnabledTrue(String phone);

    User getUserByEmailAndEnabledTrue(String email);

    @Query(
            value = "insert into virtual_wallet.authorities (username, authority) values (?1, 'ROLE_USER')",
            nativeQuery = true
    )
    @Transactional
    @Modifying
    void updateAuthorities(String username);

    @Query(
            value = "select au.authority from virtual_wallet.authorities as au where au.username = ?1",
            nativeQuery = true
    )
    List<String> getUserAuthorities(String username);

    boolean existsUserByEmail(String email);

    boolean existsUserByUsername(String username);

    boolean existsUserByPhoneAndEnabledTrue(String phone);

    Page<User> getUserByUsernameContainingAndEmailContainingAndPhoneContainingAndEnabledTrue(
            String username,
            String email,
            String phone,
            Pageable pageable);

    User findByEmailIgnoreCase(String email);
}