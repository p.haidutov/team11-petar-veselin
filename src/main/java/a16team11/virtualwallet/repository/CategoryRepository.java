package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    List<Category> getAllByUsernameAndEnabledTrue(String username);

    Category getByIdAndUsername(int id, String username);

    Category getByIdAndEnabledTrue(int id);

    boolean existsByNameAndUsername(String name, String username);
}
