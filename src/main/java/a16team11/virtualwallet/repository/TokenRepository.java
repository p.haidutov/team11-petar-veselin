package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.Token;
import org.springframework.data.repository.CrudRepository;

public interface TokenRepository extends CrudRepository<Token, String> {
    Token findByToken(String token);
}
