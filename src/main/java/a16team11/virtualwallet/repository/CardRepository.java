package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardRepository extends JpaRepository<Card, Integer> {
    Card getCardByIdAndEnabledTrue(int id);

    Card getCardByNumberAndEnabledTrue(String number);

    boolean existsCardByNumber(String number);

    List<Card> getAllByUserAndEnabledTrue(User user);
}
