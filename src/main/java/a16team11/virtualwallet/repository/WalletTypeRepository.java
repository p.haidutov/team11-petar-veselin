package a16team11.virtualwallet.repository;

import a16team11.virtualwallet.models.WalletType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletTypeRepository extends JpaRepository<WalletType, Integer> {
    WalletType getByType(String type);
}
