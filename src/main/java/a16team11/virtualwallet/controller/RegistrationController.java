package a16team11.virtualwallet.controller;

import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.UserCreationDto;
import a16team11.virtualwallet.service.common.TokenService;
import a16team11.virtualwallet.service.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserService userService;
    private TokenService tokenService;
    private DtoMapper dtoMapper;

    @Autowired
    public RegistrationController(UserService userService, TokenService tokenService,
                                  DtoMapper dtoMapper) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/sign-up")
    public String showSignUpPage(Model model) {
        model.addAttribute("user", new UserCreationDto());
        return "sign-up";
    }

    @PostMapping("/sign-up")
    public String registerUser(@Valid @ModelAttribute UserCreationDto user, Model model) {
        User registeredUser = dtoMapper.fromUserCreationDtoToUser(user);
        userService.createUser(registeredUser);
        model.addAttribute("email", registeredUser.getEmail());
        return "successfulRegistration";
    }

    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {
        tokenService.confirmToken(confirmationToken);
        modelAndView.setViewName("accountVerified");
        return modelAndView;
    }
}