package a16team11.virtualwallet.controller;

import a16team11.virtualwallet.models.MixedTransaction;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.service.PageHelper;
import a16team11.virtualwallet.service.common.TransactionService;
import a16team11.virtualwallet.service.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.IOException;
import java.nio.file.attribute.FileTime;
import java.security.Principal;

import static a16team11.virtualwallet.utils.Constants.*;

@Controller
public class AdminController {

    private UserService userService;
    private TransactionService transactionService;

    @Autowired
    public AdminController(UserService userService, TransactionService transactionService) {
        this.userService = userService;
        this.transactionService = transactionService;
    }

    @GetMapping("/users")
    public String findAll(Model model, Principal principal,
                          @RequestParam(defaultValue = "") String username,
                          @RequestParam(defaultValue = "") String email,
                          @RequestParam(defaultValue = "") String phone,
                          @RequestParam(defaultValue = "1") int page,
                          @RequestParam(defaultValue = "id") String sortBy) {

        Page<User> users = userService.getPageByFields(
                username, email, phone, PageRequest.of(page - 1, DEFAULT_USER_PAGE_SIZE, Sort.Direction.ASC, sortBy.toLowerCase()));
        model.addAttribute("username", username);
        model.addAttribute("email", email);
        model.addAttribute("phone", phone);
        model.addAttribute("data", users);
        model.addAttribute("currentPage", page);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        return "users";
    }

    @PostMapping("/save")
    public String save(User user, Principal principal, MultipartFile multipartFile) throws IOException {
        userService.update(user.getId(), user, principal.getName(), multipartFile);
        return "redirect:/users";
    }

    @GetMapping("/delete")
    public String delete(User user, Principal principal) {
        userService.delete(user.getId(), principal.getName());
        return "redirect:/users";
    }

    @GetMapping("/findOne")
    @ResponseBody
    public User findOne(int id) {
        return userService.getUserById(id);
    }

    @GetMapping("/all-transactions")
    public String showAllTransactions(Model model, Principal principal,
                                      @RequestParam(defaultValue = "") String fromUsername,
                                      @RequestParam(defaultValue = "") String toUsername,
                                      @RequestParam(defaultValue = "2020-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String startDate,
                                      @RequestParam(defaultValue = "2030-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String endDate,
                                      @RequestParam(defaultValue = "1") int page,
                                      @RequestParam(defaultValue = "DateDesc") String sortBy) throws Exception {
        Page<MixedTransaction> allTransactions = transactionService.getAllMixedTransactions(principal.getName(),
                fromUsername, toUsername, startDate, endDate, sortBy,
                PageRequest.of(page - 1, DEFAULT_TRANSACTION_PAGE_SIZE));

        model.addAttribute("pageNumbers", PageHelper.getPageNumbers(allTransactions.getTotalPages()));
        model.addAttribute("transactions", allTransactions);
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("fromUsername", fromUsername);
        model.addAttribute("toUsername", toUsername);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("sortBy", sortBy);
        return "all-transactions";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }
}