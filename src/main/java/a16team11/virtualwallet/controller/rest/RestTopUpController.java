package a16team11.virtualwallet.controller.rest;

import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.TopUpDto;
import a16team11.virtualwallet.service.common.TopUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/top-ups")
public class RestTopUpController {
    private TopUpService topUpService;
    private DtoMapper mapper;

    @Autowired
    public RestTopUpController(TopUpService topUpService, DtoMapper mapper) {
        this.topUpService = topUpService;
        this.mapper = mapper;
    }

    @PostMapping()
    public ResponseEntity<?> topUp(@RequestHeader("idempotencyKey") String key,
                                   Principal principal,
                                   @RequestBody TopUpDto topUpDto) {

        TopUp topUp = mapper.fromTopUpDtoToTopUp(topUpDto, key);
        topUpService.topUp(principal.getName(), topUp);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
