package a16team11.virtualwallet.controller.rest;


import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.UserCreationDto;
import a16team11.virtualwallet.models.dto.UserUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import a16team11.virtualwallet.service.common.CategoryService;
import a16team11.virtualwallet.service.common.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;


@RestController
@RequestMapping("/api/users")
public class RestUserController {
    private UserService userService;
    private DtoMapper mapper;

    public RestUserController(UserService userService,
                              DtoMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping
    //admin method
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/")
    public User getByField(@RequestParam(defaultValue = "0") int id,
                           @RequestParam(defaultValue = "") String username,
                           @RequestParam(defaultValue = "") String phone,
                           @RequestParam(defaultValue = "") String email) {
        return userService.getByField(id, username, phone, email);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable int id,
                                    @RequestBody @Valid UserUpdateDto userUpdateDto,
                                    Principal principal,
                                    MultipartFile multipartFile) throws IOException {
        User user = mapper.fromUserUpdateDtoToUser(userUpdateDto);
        userService.update(id, user, principal.getName(), multipartFile);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/registration")
    public ResponseEntity<?> create(@RequestBody @Valid UserCreationDto userCreationDto) {
        User user = mapper.fromUserCreationDtoToUser(userCreationDto);

        userService.createUser(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id, Principal principal) {
        userService.delete(id, principal.getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @GetMapping("/key")
    public String getKey() {
        return userService.getKey();
    }
}
