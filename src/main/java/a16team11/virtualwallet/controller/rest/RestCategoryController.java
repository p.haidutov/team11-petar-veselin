package a16team11.virtualwallet.controller.rest;

import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.dto.CategoryDto;
import a16team11.virtualwallet.service.common.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class RestCategoryController {

    private CategoryService categoryService;

    public RestCategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<List<Category>> getAllCategoriesByUsername(Principal principal) {
        List<Category> categories = categoryService.getAllByUsername(principal.getName());
        return ResponseEntity.status(HttpStatus.OK).body(categories);
    }

    @PostMapping
    public ResponseEntity<?> createCategory(@RequestBody @Valid CategoryDto categoryDto,
                                            Principal principal) {
        categoryService.create(categoryDto.getName(), principal.getName());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable int id,
                                            @RequestBody @Valid CategoryDto categoryDto,
                                            Principal principal) {
        categoryService.update(id, categoryDto.getName(), principal.getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable int id,
                                            Principal principal) {
        categoryService.delete(id, principal.getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}