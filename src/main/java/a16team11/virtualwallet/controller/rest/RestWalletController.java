package a16team11.virtualwallet.controller.rest;

import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.WalletDto;
import a16team11.virtualwallet.service.common.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/wallets")
public class RestWalletController {
    private WalletService walletService;
    private DtoMapper mapper;

    @Autowired
    public RestWalletController(WalletService walletService, DtoMapper mapper) {
        this.walletService = walletService;
        this.mapper = mapper;
    }

    @GetMapping("/")
    public ResponseEntity<Wallet> getByField(@RequestParam(defaultValue = "0") int id,
                                             @RequestParam(defaultValue = "") String name,
                                             Principal principal) {
        Wallet wallet = walletService.getWalletByField(id, name, principal.getName());
        return ResponseEntity.status(HttpStatus.OK).body(wallet);
    }

    @PostMapping()
    public ResponseEntity<?> create(@RequestBody @Valid WalletDto walletDto,
                                    Principal principal) {
        Wallet wallet = mapper.fromWalletDtoToWallet(walletDto, principal.getName());
        walletService.create(wallet);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable int id,
                                    @RequestBody @Valid WalletDto walletDto,
                                    Principal principal) {
        Wallet wallet = mapper.fromWalletDtoToWallet(walletDto, principal.getName());
        walletService.update(wallet.getName(), id, principal.getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id,
                                    Principal principal) {
        walletService.delete(id, principal.getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
