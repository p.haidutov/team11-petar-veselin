package a16team11.virtualwallet.controller.rest;

import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.dto.CardDto;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.service.common.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/cards")
public class RestCardController {
    private CardService cardService;
    private DtoMapper mapper;

    @Autowired
    public RestCardController(CardService cardService, DtoMapper mapper) {
        this.cardService = cardService;
        this.mapper = mapper;
    }

    @GetMapping("/")
    public ResponseEntity<Card> getByField(@RequestParam(defaultValue = "0") int id,
                                           @RequestParam(defaultValue = "") String number,
                                           Principal principal) {
        Card card = cardService.getCardByField(id, number, principal.getName());
        return ResponseEntity.status(HttpStatus.OK).body(card);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable int id,
                                    @RequestBody @Valid CardDto cardDto,
                                    Principal principal) {
        Card cardUpdate = mapper.fromCardCreationDtoToCard(cardDto, principal.getName());
        cardService.update(id, cardUpdate, principal.getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody @Valid CardDto cardDto,
                                    Principal principal) {
        Card card = mapper.fromCardCreationDtoToCard(cardDto, principal.getName());
        cardService.create(card);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id,
                                    Principal principal) {
        cardService.delete(id, principal.getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
