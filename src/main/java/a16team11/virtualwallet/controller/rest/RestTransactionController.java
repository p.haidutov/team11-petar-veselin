package a16team11.virtualwallet.controller.rest;

import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.MixedTransaction;
import a16team11.virtualwallet.models.Transaction;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.TransactionCreationDto;
import a16team11.virtualwallet.service.common.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static a16team11.virtualwallet.utils.Constants.REST_TRANSACTION_PAGE_SIZE;

@RestController
@RequestMapping("/api/transactions")
public class RestTransactionController {
    private TransactionService transactionService;
    private DtoMapper mapper;

    @Autowired
    public RestTransactionController(TransactionService transactionService, DtoMapper mapper) {
        this.transactionService = transactionService;
        this.mapper = mapper;
    }

    @GetMapping("/all")
    //Admin method
    public ResponseEntity<Page<MixedTransaction>>
    getAllTransactions(Principal principal,
                       @RequestParam(defaultValue = "") String fromUsername,
                       @RequestParam(defaultValue = "") String toUsername,
                       @RequestParam(defaultValue = "2020-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String startDate,
                       @RequestParam(defaultValue = "2030-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String endDate,
                       @RequestParam(defaultValue = "0") int page,
                       @RequestParam(defaultValue = "DateDesc") String sortBy) throws Exception {

        Page<MixedTransaction> allTransactions =
                transactionService.getAllMixedTransactions(principal.getName(),
                        fromUsername, toUsername, startDate, endDate, sortBy,
                        PageRequest.of(page, REST_TRANSACTION_PAGE_SIZE));

        return ResponseEntity.status(HttpStatus.OK).body(allTransactions);
    }

    @GetMapping
    public ResponseEntity<Page<MixedTransaction>>
    getUserTransactions(Principal principal,
                        @ModelAttribute("updatedCategory") Category category,
                        @RequestParam(defaultValue = "") String toUsername,
                        @RequestParam(defaultValue = "0") int categoryId,
                        @RequestParam(defaultValue = "2020-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String startDate,
                        @RequestParam(defaultValue = "2030-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String endDate,
                        @RequestParam(defaultValue = "0") int page,
                        @RequestParam(defaultValue = "DateDesc") String sortBy) throws Exception {

        Page<MixedTransaction> myTransactions =
                transactionService.getMixedTransactionsByUsername(principal.getName(), toUsername,
                        categoryId, startDate, endDate, sortBy,
                        PageRequest.of(page, REST_TRANSACTION_PAGE_SIZE));

        return ResponseEntity.status(HttpStatus.OK).body(myTransactions);
    }

    @GetMapping("/top5")
    public ResponseEntity<List<MixedTransaction>> getTop5UserTransactions(Principal principal) {
        List<MixedTransaction> list = transactionService.getTop5MixedTransactions(principal.getName());
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    @PostMapping("/{walletId}")
    public ResponseEntity<?> createTransaction(@RequestHeader(name = "idempotencyKey") String key,
                                                                @PathVariable(name = "walletId") int fromWalletId,
                                                                @RequestBody TransactionCreationDto transactionCreationDto,
                                                                Principal principal) {
        Transaction transaction = mapper.fromTransactionCreationDtoToTransaction(
                transactionCreationDto, fromWalletId, principal.getName());

        transactionService.createTransaction(transaction, key,
                principal.getName(), fromWalletId);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}