package a16team11.virtualwallet.controller;

import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.dto.CategoryDto;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.service.common.CategoryService;
import a16team11.virtualwallet.service.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class CategoryController {

    private UserService userService;
    private CategoryService categoryService;
    private DtoMapper mapper;

    @Autowired
    public CategoryController(UserService userService, CategoryService categoryService, DtoMapper mapper) {
        this.userService = userService;
        this.categoryService = categoryService;
        this.mapper = mapper;
    }

    @GetMapping("/transactions/addCategory")
    public String showAddNewCategoryPage(Model model, Principal principal) {
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("categories", categoryService.getAllByUsername(principal.getName()));
        model.addAttribute("newCategory", new CategoryDto());
        return "transactions";
    }

    @PostMapping("/transactions/addCategory")
    public String addNewCategory(Model model, Principal principal,
                                 @ModelAttribute("newCategory") @Valid CategoryDto categoryDto) {

        categoryService.create(categoryDto.getName(), principal.getName());
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        return "redirect:/transactions";
    }

    @GetMapping("/transactions/updateCategory/{id}")
    public String showUpdateCategoryPage(@PathVariable int id, Model model,
                                         Principal principal) {

        CategoryDto category = mapper.fromCategoryToCategoryDto(categoryService.getById(id));
        model.addAttribute("updatedCategory", category);
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        return "transactions";
    }

    @PostMapping("/transactions/updateCategory/{id}")
    public String updateCategory(@PathVariable int id, Principal principal,
                                 @ModelAttribute("updatedCategory") @Valid CategoryDto categoryDto) {

        categoryService.update(id, categoryDto.getName(), principal.getName());
        return "redirect:/transactions";
    }

    @GetMapping("/transactions/deleteCategory/{id}")
    public String deleteCategory(@PathVariable int id, Principal principal) {
        categoryService.delete(id, principal.getName());
        return "redirect:/transactions";
    }
}
