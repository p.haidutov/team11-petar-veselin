package a16team11.virtualwallet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StaticPagesController {

    @GetMapping("/")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/send-money")
    public String showSendMoneyPage() {
        return "send-money";
    }

    @GetMapping("/about")
    public String showAboutPage() {
        return "about";
    }

    @GetMapping("/contact")
    public String showContactPage() {
        return "contact";
    }

    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }
}