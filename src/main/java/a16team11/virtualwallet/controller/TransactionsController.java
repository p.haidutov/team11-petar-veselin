package a16team11.virtualwallet.controller;

import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.MixedTransaction;
import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.Transaction;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.TopUpDto;
import a16team11.virtualwallet.models.dto.TransactionCreationDto;
import a16team11.virtualwallet.service.PageHelper;
import a16team11.virtualwallet.models.dto.UserDisplayDto;
import a16team11.virtualwallet.service.common.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static a16team11.virtualwallet.utils.Constants.DEFAULT_TRANSACTION_PAGE_SIZE;

@Controller
@SessionAttributes({"transactionCreationDto", "topUpDto"})
public class TransactionsController {

    private UserService userService;
    private TransactionService transactionService;
    private TopUpService topUpService;
    private CategoryService categoryService;
    private DtoMapper mapper;
    private WalletService walletService;
    private CardService cardService;

    @Autowired
    public TransactionsController(UserService userService, TransactionService transactionService, DtoMapper mapper,
                                  CategoryService categoryService, WalletService walletService,
                                  TopUpService topUpService, CardService cardService) {
        this.userService = userService;
        this.transactionService = transactionService;
        this.categoryService = categoryService;
        this.mapper = mapper;
        this.walletService = walletService;
        this.topUpService = topUpService;
        this.cardService = cardService;
    }

    @ModelAttribute
    public TransactionCreationDto transactionCreationDto() {
        return new TransactionCreationDto();
    }

    @ModelAttribute
    public TopUpDto topUpDto() {
        return new TopUpDto();
    }

    @GetMapping("/transactions")
    public String showTransactionsPage(Model model, Principal principal, @ModelAttribute("updatedCategory") Category category,
                                       @RequestParam(defaultValue = "") String toUsername,
                                       @RequestParam(defaultValue = "0") int categoryId,
                                       @RequestParam(defaultValue = "2020-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String startDate,
                                       @RequestParam(defaultValue = "2030-01-01") @DateTimeFormat(pattern = "dd-MM-yyyy") String endDate,
                                       @RequestParam(defaultValue = "1") int page,
                                       @RequestParam(defaultValue = "DateDesc") String sortBy) throws Exception {

        Page<MixedTransaction> myTransactions = transactionService.getMixedTransactionsByUsername(principal.getName(), toUsername,
                categoryId, startDate, endDate, sortBy,
                PageRequest.of(page - 1, DEFAULT_TRANSACTION_PAGE_SIZE));

        model.addAttribute("pageNumbers", PageHelper.getPageNumbers(myTransactions.getTotalPages()));
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("categories", categoryService.getAllByUsername(principal.getName()));
        model.addAttribute("transactionList", myTransactions);
        model.addAttribute("toUsername", toUsername);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("newCategory", new Category());

        return "transactions";
    }

    @GetMapping("/deposit-money")
    public String showDepositMoneyPage(@ModelAttribute TopUpDto topUpDto,
                                       Model model, Principal principal) {
        topUpDto.setKey(userService.getKey());
        User user = userService.getUserByUsername(principal.getName());
        model.addAttribute("topUpDto", topUpDto);
        model.addAttribute("user", user);
        model.addAttribute("cards", cardService.getUserCards(user));
        return "deposit-money";
    }

    @GetMapping("/deposit-money-confirm")
    public String showDepositMoneyConfirmPage(@ModelAttribute TopUpDto topUpDto,
                                              Model model, Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        model.addAttribute("topUpDto", topUpDto);
        model.addAttribute("user", user);
        model.addAttribute("wallets", walletService.getAllByUser(user));
        return "deposit-money-confirm";
    }

    @PostMapping("/deposit-money-confirm")
    public String depositMoney(Model model, Principal principal,
                               @ModelAttribute @Valid TopUpDto topUpDto,
                               SessionStatus status) {
        TopUp topUp = mapper.fromTopUpDtoToTopUp(topUpDto, topUpDto.getKey());
        topUpService.topUp(principal.getName(), topUp);
        status.setComplete();
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        return "deposit-money-success";
    }

    @GetMapping("/deposit-money-success")
    public String showDepositMoneySuccessPage(Model model, Principal principal) {
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        return "deposit-money-success";
    }

    @GetMapping("/send-money-process")
    public String showSendMoneyProcessPage(@ModelAttribute TransactionCreationDto transactionCreationDto,
                                           Model model, Principal principal,
                                           @RequestParam(defaultValue = "1") int page) {
        transactionCreationDto.setKey(userService.getKey());
        List<User> users = userService.getAll();

        Page<UserDisplayDto> userDisplayDtos =
                mapper.fromUserToUserDisplayDto(users, PageRequest.of(page - 1, DEFAULT_TRANSACTION_PAGE_SIZE));

        model.addAttribute("data", userDisplayDtos);
        model.addAttribute("pageNumbers", PageHelper.getPageNumbers(userDisplayDtos.getTotalPages()));
        model.addAttribute("transaction", transactionCreationDto);
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("wallet", walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        return "send-money-process";
    }

    @GetMapping("/send-money-confirm")
    public String showSendMoneyConfirmPage(@ModelAttribute TransactionCreationDto transactionCreationDto,
                                           Model model, Principal principal) {
        model.addAttribute("transaction", transactionCreationDto);
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("categories", categoryService.getAllByUsername(principal.getName()));
        model.addAttribute("wallets", walletService.getAllByUser(userService.getUserByUsername(principal.getName())));
        return "send-money-confirm";
    }

    @PostMapping("/send-money-confirm")
    public String confirmSendMoney(Model model, Principal principal,
                                   @ModelAttribute @Valid TransactionCreationDto transactionCreationDto,
                                   SessionStatus status) {

        Transaction transaction = mapper.fromTransactionCreationDtoToTransaction(
                transactionCreationDto, principal.getName());

        transactionService.createTransaction(transaction, transactionCreationDto.getKey(),
                principal.getName(), transactionCreationDto.getFromWalletId());
        status.setComplete();
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("categories", categoryService.getAllByUsername(principal.getName()));
        return "send-money-success";
    }

    @GetMapping("/send-money-success")
    public String showSendMoneySuccessPage(Model model, Principal principal) {
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        return "send-money-success";
    }
}