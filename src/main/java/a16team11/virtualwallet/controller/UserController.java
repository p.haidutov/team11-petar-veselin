package a16team11.virtualwallet.controller;

import a16team11.virtualwallet.models.MixedTransaction;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.UserUpdateDto;
import a16team11.virtualwallet.service.common.TransactionService;
import a16team11.virtualwallet.service.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Controller
public class UserController {

    private UserService userService;
    private TransactionService transactionService;
    private DtoMapper mapper;

    @Autowired
    public UserController(UserService userService, TransactionService transactionService,
                          DtoMapper mapper) {
        this.userService = userService;
        this.transactionService = transactionService;
        this.mapper = mapper;
    }

    @GetMapping("/dashboard")
    public String showDashboardPage(Model model, Principal principal) {
        List<MixedTransaction> transactionViewDtoList = transactionService.getTop5MixedTransactions(principal.getName());

        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("transactionList", transactionViewDtoList);
        return "dashboard";
    }

    @GetMapping("/profile")
    public String getCurrentUserProfile(Model model, Principal principal) {
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("updatedUser", new User());
        return "profile";
    }

    @GetMapping("/updateUser")
    public String showUpdateUserPage(Model model, Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        UserUpdateDto updatedUser = mapper.fromUserToUserUpdateDto(user);
        model.addAttribute("updatedUser", updatedUser);
        return "profile";
    }

    @PostMapping("/updateUser")
    public String updateUser(Principal principal, @Valid @ModelAttribute("updatedUser") UserUpdateDto updatedUser, Model model,
                             @RequestParam MultipartFile file, BindingResult bindingResult) throws IOException {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Wrong fields typed!");
            return "profile";
        }

        User user = mapper.fromUserUpdateDtoToUser(updatedUser);
        User loggedUser = userService.getUserByUsername(principal.getName());
        userService.update(loggedUser.getId(), user, principal.getName(), file);
        return "redirect:/profile";
    }

    @GetMapping("/changeUserPassword")
    public String showPasswordPage(Model model, Principal principal) {
        model.addAttribute("userCP", userService.getUserByUsername(principal.getName()));
        return "profile";
    }

    @PostMapping("/changeUserPassword")
    public String changePassword(Model model, Principal principal, @RequestParam(value = "oldPassword") String oldPassword,
                                 @RequestParam(value = "newPassword") String newPassword,
                                 @RequestParam(value = "confirmNewPassword") String confirmNewPassword) {

        userService.updatePassword(oldPassword, newPassword, confirmNewPassword, principal.getName());
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        return "Successfully-changed-password";
    }
}
