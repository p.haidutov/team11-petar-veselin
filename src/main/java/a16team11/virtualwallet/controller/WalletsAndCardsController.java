package a16team11.virtualwallet.controller;

import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.models.dto.CardDto;
import a16team11.virtualwallet.models.dto.DtoMapper;
import a16team11.virtualwallet.models.dto.WalletDto;
import a16team11.virtualwallet.service.common.CardService;
import a16team11.virtualwallet.service.common.UserService;
import a16team11.virtualwallet.service.common.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class WalletsAndCardsController {
    private UserService userService;
    private WalletService walletService;
    private CardService cardService;
    private DtoMapper mapper;

    @Autowired
    public WalletsAndCardsController(UserService userService, WalletService walletService, CardService cardService,
                                     DtoMapper mapper) {
        this.userService = userService;
        this.walletService = walletService;
        this.cardService = cardService;
        this.mapper = mapper;
    }

    @GetMapping("/wallets-and-cards")
    public String showWalletsAndCardsPage(Model model, Principal principal,
                                          @ModelAttribute("updatedCard") CardDto cardDto,
                                          @ModelAttribute("updatedWallet") WalletDto walletDto) {
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("primaryWallet",
                walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        model.addAttribute("wallets",
                walletService.getAllByUser(userService.getUserByUsername(principal.getName())));
        model.addAttribute("cards",
                cardService.getUserCards(userService.getUserByUsername(principal.getName())));
        model.addAttribute("newCard", new CardDto());
        model.addAttribute("newWallet", new WalletDto());
        return "wallets-and-cards";
    }

    @GetMapping("/wallets-and-cards/addCard")
    public String showAddNewCardPage(Model model, Principal principal) {
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("primaryWallet",
                walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        model.addAttribute("wallets",
                walletService.getAllByUser(userService.getUserByUsername(principal.getName())));
        model.addAttribute("cards",
                cardService.getUserCards(userService.getUserByUsername(principal.getName())));
        model.addAttribute("newCard", new CardDto());
        return "wallets-and-cards";
    }

    @PostMapping("/wallets-and-cards/addCard")
    public String addNewCard(Model model, Principal principal,
                             @ModelAttribute("newCard") @Valid CardDto cardDto) {
        Card card = mapper.fromCardCreationDtoToCard(cardDto, principal.getName());
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("primaryWallet",
                walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        cardService.create(card);
        return "redirect:/wallets-and-cards";
    }

    @GetMapping("/wallets-and-cards/updateCard/{id}")
    public String showUpdateCardPage(@PathVariable int id, Model model, Principal principal,
                                     @ModelAttribute("updatedWallet") WalletDto walletDto) {
        Card card = cardService.getCardById(id);
        CardDto cardDto = mapper.fromCardToCardDto(card, principal.getName());
        model.addAttribute("updatedCard", cardDto);
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("cards",
                cardService.getUserCards(userService.getUserByUsername(principal.getName())));
        model.addAttribute("primaryWallet",
                walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        model.addAttribute("newCard", new CardDto());
        model.addAttribute("newWallet", new WalletDto());

        return "wallets-and-cards";
    }

    @PostMapping("/wallets-and-cards/updateCard/{id}")
    public String updateCard(@PathVariable int id, Principal principal,
                             @ModelAttribute("updatedCard") CardDto cardDto) {
        Card card = mapper.fromCardCreationDtoToCard(cardDto, principal.getName());
        cardService.update(id, card, principal.getName());
        return "redirect:/wallets-and-cards";
    }

    @GetMapping("/wallets-and-cards/deleteCard/{id}")
    public String deleteCard(@PathVariable int id, Principal principal) {
        cardService.delete(id, principal.getName());
        return "redirect:/wallets-and-cards";
    }

    @GetMapping("/wallets-and-cards/addWallet")
    public String showAddNewWalletPage(Model model, Principal principal) {
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("primaryWallet",
                walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        model.addAttribute("wallets",
                walletService.getAllByUser(userService.getUserByUsername(principal.getName())));
        model.addAttribute("newWallet", new WalletDto());
        return "wallets-and-cards";
    }

    @PostMapping("/wallets-and-cards/addWallet")
    public String addNewWallet(Model model, Principal principal,
                               @ModelAttribute("newWallet") WalletDto newWallet) {

        Wallet wallet = mapper.fromWalletDtoToWallet(newWallet, principal.getName());
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("primaryWallet",
                walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        walletService.create(wallet);
        return "redirect:/wallets-and-cards";
    }

    @GetMapping("/wallets-and-cards/updateWallet/{id}")
    public String showUpdateWalletPage(@PathVariable int id, Model model, Principal principal) {
        Wallet wallet = walletService.getWalletById(id);
        WalletDto walletDto = mapper.fromWalletToWalletDto(wallet, principal.getName());

        model.addAttribute("updatedWallet", walletDto);
        model.addAttribute("user", userService.getUserByUsername(principal.getName()));
        model.addAttribute("wallets",
                walletService.getAllByUser(userService.getUserByUsername(principal.getName())));
        model.addAttribute("primaryWallet",
                walletService.getPrimaryWallet(userService.getUserByUsername(principal.getName())));
        model.addAttribute("cards",
                cardService.getUserCards(userService.getUserByUsername(principal.getName())));
        model.addAttribute("newCard", new CardDto());
        model.addAttribute("newWallet", new WalletDto());
        return "wallets-and-cards";
    }

    @PostMapping("/wallets-and-cards/updateWallet/{id}")
    public String updateWallet(@PathVariable int id, Principal principal,
                               @ModelAttribute("updatedWallet") @Valid WalletDto walletDto) {

        Wallet wallet = mapper.fromWalletDtoToWallet(walletDto, principal.getName());
        walletService.update(wallet.getName(), id, principal.getName());
        return "redirect:/wallets-and-cards";
    }

    @GetMapping("/wallets-and-cards/deleteWallet/{id}")
    public String deleteWallet(@PathVariable int id, Principal principal) {
        walletService.delete(id, principal.getName());
        return "redirect:/wallets-and-cards";
    }
}