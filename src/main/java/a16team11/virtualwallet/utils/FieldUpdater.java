package a16team11.virtualwallet.utils;

import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.Transaction;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;

@Component
public class FieldUpdater {

    public FieldUpdater() {
    }

    public User updateUserFields(User user, User userUpdate, MultipartFile file) throws IOException {
        user.setEmail(userUpdate.getEmail());
        user.setPhone(userUpdate.getPhone());
        user.setFirstName(userUpdate.getFirstName());
        user.setLastName(userUpdate.getLastName());
        if (file != null && file.getSize() > 0) {
            user.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
        }
        user.setBlocked(userUpdate.isBlocked());
        return user;
    }

    public Card updateCardFields(Card card, Card cardUpdate) {
        if (cardUpdate.getNumber() != null) {
            card.setNumber(cardUpdate.getNumber());
        }
        if (cardUpdate.getSecurityCode() != null) {
            card.setSecurityCode(cardUpdate.getSecurityCode());
        }
        if (cardUpdate.getExp_date() != null) {
            card.setExp_date(cardUpdate.getExp_date());
        }
        if (cardUpdate.getOwnerName() != null) {
            card.setOwnerName(cardUpdate.getOwnerName());
        }

        return card;
    }

    public Transaction updateTransactionFields(Transaction transaction, String key,
                                               Wallet toWallet) {
        transaction.setIdempotencyKey(key);
        transaction.setToWallet(toWallet);
        transaction.setDate(new Date());
        transaction.setCurrency("BGN");
        return transaction;
    }
}
