package a16team11.virtualwallet.utils;

public class Constants {
    //Payment API
    public static final String PAYMENT_API_URI = "http://localhost:8081/payment";
    public static final String X_API_KEY = "x-api-key";
    public static final String INSUFFICIENT_FUNDS_ERROR = "Insufficient funds.";
    public static final String MISSING_IDEMPOTENCY_KEY_ERROR = "Missing idempotency key in header.";

    //Transactions
    public static final String TRANSACTION_ALREADY_PROCESSED_ERROR = "This transaction was already processed.";
    public static final String NO_TRANSACTIONS_UNDER_CATEGORY_ERROR = "There are no transactions under this category.";
    public static final String NO_TRANSACTIONS_FROM_USER_ERROR = "You have not made any transactions yet.";
    public static final String INVALID_CATEGORY_TRANSACTION_ERROR = "You cannot make a transaction with an invalid category";

    public static final String INCOMING_TYPE = "Incoming";
    public static final String OUTGOING_TYPE = "Outgoing";
    public static final String DEPOSIT_CATEGORY = "Deposit";


    //Cards
    public static final String NO_SUCH_CARD_ERROR = "No such card.";
    public static final String EMPTY_CARDS_LIST_ERROR = "No such cards in the database.";
    public static final String DUPLICATE_CARD_ERROR = "A card with this number already exists.";
    public static final String CARD_DOES_NOT_BELONG_TO_USER_ERROR = "Card does not belong to user.";
    public static final String INVALID_CARD_FORMAT_ERROR = "Invalid card format.";

    //Categories
    public static final String DUPLICATE_CATEGORY = "You already have a category with this name";
    public static final String NO_SUCH_CATEGORY_ERROR = "No such category";
    public static final String GENERAL_CATEGORY = "General";
    public static final int TOP_UPS_ONLY = 1;
    public static final int ALL_TRANSACTIONS = 0;

    //Wallets
    public static final String EMPTY_WALLETS_LIST_ERROR = "No such wallets in the database.";
    public static final String NO_SUCH_WALLET_ERROR = "No such wallet.";
    public static final String INSUFFICIENT_FUNDS_IN_WALLET_ERROR =
            "Insufficient funds. Please add more money to your wallet to make this transaction.";
    public static final String USER_ALREADY_HAS_SUCH_WALLET_ERROR = "You already have a wallet with this name";
    public static final String WALLET_DOES_NOT_BELONG_TO_USER_ERROR = "Wallet does not belong to user.";
    public static final String PRIMARY_WALLET_NAME = "Primary Wallet";
    public static final String PRIMARY = "primary";
    public static final String SECONDARY = "secondary";

    //Users
    public static final String NO_SUCH_USER_ERROR = "No such user.";
    public static final String EMPTY_USERS_LIST_ERROR = "No such users in the database.";
    public static final String EMAIL_ALREADY_USED_ERROR = "Email already in use.";
    public static final String PHONE_ALREADY_USED_ERROR = "Phone already in use.";
    public static final String USERNAME_ALREADY_USED_ERROR = "Username already in use.";
    public static final String USER_IS_BLOCKED_ERROR =
            "User is blocked and cannot make transactions and/or update card details.";
    public static final String ADMIN = "ROLE_ADMIN";
    public static final String USER_NOT_AUTHORIZED_TO_ACCESS_THIS_RESOURCE =
            "User is not authorized to access this resource";

    //Email sender
    public static final String URL = "http://localhost:8080/confirm-account?token=";
    public static final String EMAIL_SENDER = "toziezaigri@gmail.com";
    public static final String CONFIRM_ACCOUNT_TITLE = "Complete Registration!";
    public static final String CONFIRM_ACCOUNT_MESSAGE = "To confirm you account at Virtual Wallet website, please click here: ";
    public static final String INVALID_TOKEN_MESSAGE = "The link is invalid or broken!";

    //Paging
    public static final int DEFAULT_TRANSACTION_PAGE_SIZE = 5;
    public static final int REST_TRANSACTION_PAGE_SIZE = 100;
    public static final int DEFAULT_USER_PAGE_SIZE = 5;

    //Passwords
    public static final String PASSWORDS_DO_NOT_MATCH_ERROR = "Passwords do not match.";
    public static final String INVALID_OLD_PASSWORD_ERROR = "Invalid old password.";

    //Exception Handling
    public static final String INSUFFICIENT_FUNDS_TO_COMPLETE_THE_TRANSACTION = "Insufficient funds to complete the transaction.";
    public static final String OOPS_INVALID_PAGE_ERROR = "Oops! Seems like the page you are looking for is gone.";
    public static final String OOPS_INVALID_REQUEST = "Oops! Seems like your request is not a valid one.";
    public static final String OOPS_SOMETHING_WENT_WRONG = "Oops! Seems like something went wrong... It's not you, it's me.";
    public static final String FILE_SIZE_EXCEEDED_ERROR = "The file exceeds its maximum permitted size of 10MB.";
    public static final String STATUS = "status";
    public static final String ERROR_MSG = "errorMsg";
    public static final String ERROR_STRING = "error";

    public static final String PHONE_REGEX = "^\\+(?:[0-9] ?){6,14}[0-9]$";
    public static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    public static final String CARD_REGEX = "^4[0-9]{12}(?:[0-9]{3})?$";
}
