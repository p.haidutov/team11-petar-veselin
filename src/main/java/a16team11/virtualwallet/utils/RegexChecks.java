package a16team11.virtualwallet.utils;

import java.util.regex.Pattern;

import static a16team11.virtualwallet.utils.Constants.*;

public class RegexChecks {
    public static boolean checkIfStringIsValidPhone(String phone) {
        Pattern pattern = Pattern.compile(PHONE_REGEX);
        return pattern.matcher(phone).matches();
    }

    public static boolean checkIfStringIsValidEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        return pattern.matcher(email).matches();
    }

    public static boolean checkIfStringIsValidCard(String card){
        Pattern pattern = Pattern.compile(CARD_REGEX);
        return pattern.matcher(card).matches();
    }
}
