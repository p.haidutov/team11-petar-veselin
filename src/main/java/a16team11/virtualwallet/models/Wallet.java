package a16team11.virtualwallet.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Entity
@Table(name = "wallets")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @PositiveOrZero
    private int id;

    @OneToOne
    @JoinColumn(name = "type_id")
    private WalletType walletType;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "name")
    private String name;

    @JsonBackReference(value = "walletRef")
    @ManyToOne
    private User user;

    @Column(name = "balance")
    private BigDecimal balance;

    public Wallet() {
        balance = BigDecimal.valueOf(0.0);
        enabled = true;
    }

    public Wallet(int id, boolean enabled, String name, BigDecimal balance) {
        this.id = id;
        this.enabled = enabled;
        this.name = name;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void addBalance(BigDecimal sumToAdd) {
        balance = balance.add(sumToAdd);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public WalletType getWalletType() {
        return walletType;
    }

    public void setWalletType(WalletType walletType) {
        this.walletType = walletType;
    }
}
