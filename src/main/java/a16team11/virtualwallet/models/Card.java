package a16team11.virtualwallet.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;

@Entity
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @PositiveOrZero
    private int id;

    @JsonBackReference(value = "cardRef")
    @ManyToOne(cascade = CascadeType.ALL)
    private User user;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "number")
    private String number;

    @Column(name = "exp_date")
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "mm/dd/yyyy")
    private Date exp_date;

    @Column(name = "security_code")
    private String securityCode;

    @Column(name = "owner_name")
    private String ownerName;

    public Card() {
        enabled = true;
    }

    public Card(int id, boolean enabled, String number, Date exp_date, String securityCode, String ownerName) {
        this.id = id;
        this.enabled = enabled;
        this.number = number;
        this.exp_date = exp_date;
        this.securityCode = securityCode;
        this.ownerName = ownerName;
    }

    public Card(int id, boolean enabled, String number, String securityCode, String ownerName) {
        this.id = id;
        this.enabled = enabled;
        this.number = number;
        this.securityCode = securityCode;
        this.ownerName = ownerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getExp_date() {
        return exp_date;
    }

    public void setExp_date(Date exp_date) {
        this.exp_date = exp_date;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
