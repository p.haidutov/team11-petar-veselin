package a16team11.virtualwallet.models;

import java.util.Date;

public class ExceptionResponseBody {
    private Date date;
    private int status;
    private String error;
    private String message;

    public ExceptionResponseBody() {
        date = new Date();
    }

    public ExceptionResponseBody(int status, String error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
        date = new Date();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
