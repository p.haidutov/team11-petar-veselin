package a16team11.virtualwallet.models;

import java.math.BigDecimal;
import java.util.Date;

public class MixedTransaction {
    private Date date;
    private String from;
    private String to;
    private BigDecimal amount;
    private String type;
    private String categoryName;
    private String description;

    public MixedTransaction(Date date, String from, String to, BigDecimal amount,
                            String type, String categoryName, String description) {
        this.date = date;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.type = type;
        this.categoryName = categoryName;
        this.description = description;
    }

    public MixedTransaction() {

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
