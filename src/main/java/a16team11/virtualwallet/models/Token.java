package a16team11.virtualwallet.models;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Table(name="tokens")
@Entity
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="token_id")
    private long id;

    @Column(name= "token")
    private String token;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_date")
    private Date createdDate;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "username")
    private User user;

    @Column(name="enabled")
    private boolean enabled;

    public Token() {

    }

    public Token(User user) {
        this.user = user;
        createdDate = new Date();
        token = UUID.randomUUID().toString();
//        enabled = true;
    }

    public Token(int id, String token){
        this.id = id;
        this.token = token;
    }

    public boolean isEnabled() {
        return enabled;
    }
    public long getId() {
        return id;
    }
    public String getToken() {
        return token;
    }
    public User getUser() {
        return user;
    }
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    public void setId(long id) {
        this.id = id;
    }
    public void setToken(String confirmationToken) {
        this.token = confirmationToken;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}