package a16team11.virtualwallet.models.dto;

public class UserDisplayDto {

    private String username;
    private String picture;

    public UserDisplayDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
