package a16team11.virtualwallet.models.dto;

import a16team11.virtualwallet.service.validator.common.ValidEmail;
import a16team11.virtualwallet.service.validator.common.ValidPhoneNumber;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserUpdateDto {
    private int id;

    @ValidEmail
    @NotNull
    @NotBlank
    private String email;

    @ValidPhoneNumber
    @NotNull
    @NotBlank
    private String phone;
    private String firstName;
    private String lastName;
    private String picture;

    public UserUpdateDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
