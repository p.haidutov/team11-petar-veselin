package a16team11.virtualwallet.models.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionAdminDto {

    private int id;
    private Date date;
    private String fromUser;
    private String toUser;
    private BigDecimal amount;
    private String currency;
    private String description;

    public TransactionAdminDto() {
    }

    public TransactionAdminDto(int id, String fromUser, String toUser, BigDecimal amount,
                               String description, Date date, String currency) {
        this.id = id;
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.currency = currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
