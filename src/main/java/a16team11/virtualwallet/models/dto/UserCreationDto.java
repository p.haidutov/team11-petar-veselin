package a16team11.virtualwallet.models.dto;

import a16team11.virtualwallet.service.validator.common.PasswordMatches;
import a16team11.virtualwallet.service.validator.common.ValidEmail;
import a16team11.virtualwallet.service.validator.common.ValidPhoneNumber;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@PasswordMatches
public class UserCreationDto {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotNull
    @NotBlank
    private String password;

    @NotNull
    @NotBlank
    private String passwordConfirmation;

    @ValidEmail
    @NotNull
    @NotBlank
    private String email;

    @ValidPhoneNumber
    @NotNull
    @NotBlank
    private String phone;

    public UserCreationDto() {
    }

    public UserCreationDto(String username, String password, String passwordConfirmation, String email, String phone) {
        this.username = username;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.email = email;
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
