package a16team11.virtualwallet.models.dto;

import java.math.BigDecimal;

public class TopUpDto {
    private int id;
    private int cardId;
    private int walletId;
    private BigDecimal amount;
    private String description;
    private String currency;
    private String key;

    public TopUpDto() {
        currency = "BGN";
    }

    public TopUpDto(int id, int cardId, int walletId, BigDecimal amount, String description) {
        this.currency = "BGN";
        this.id = id;
        this.cardId = cardId;
        this.walletId = walletId;
        this.amount = amount;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
