package a16team11.virtualwallet.models.dto;

import a16team11.virtualwallet.models.*;
import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.service.PageHelper;
import a16team11.virtualwallet.service.common.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DtoMapper {
    private CardService cardService;
    private UserService userService;
    private WalletService walletService;
    private TransactionService transactionService;
    private PasswordEncoder passwordEncoder;
    private CategoryService categoryService;

    @Autowired
    public DtoMapper(CardService cardService, UserService userService, WalletService walletService,
                     PasswordEncoder passwordEncoder, CategoryService categoryService,
                     TransactionService transactionService) {
        this.cardService = cardService;
        this.userService = userService;
        this.walletService = walletService;
        this.passwordEncoder = passwordEncoder;
        this.categoryService = categoryService;
        this.transactionService = transactionService;
    }

    public Card fromCardCreationDtoToCard(CardDto cardDto, String username) {
        Card cardToCreate = new Card();
        User user = userService.getUserByUsername(username);

        cardToCreate.setNumber(cardDto.getNumber());
        cardToCreate.setSecurityCode(cardDto.getSecurityCode());
        cardToCreate.setOwnerName(cardDto.getOwnerName());
        Date date = new Date();
        try {
            date = new SimpleDateFormat("yyyy-MM").parse(cardDto.getExp_date());
        } catch (Exception e) {
            e.printStackTrace();
        }

        cardToCreate.setExp_date(date);
        cardToCreate.setUser(user);

        return cardToCreate;
    }

    public CardDto fromCardToCardDto(Card card, String username) {

        CardDto cardDto = new CardDto();
        cardDto.setId(card.getId());
        cardDto.setNumber(card.getNumber());
        cardDto.setSecurityCode(card.getSecurityCode());
        cardDto.setOwnerName(card.getOwnerName());
        cardDto.setExp_date(card.getExp_date().toString());
        cardDto.setUsername(username);

        return cardDto;
    }

    public User fromUserCreationDtoToUser(UserCreationDto userCreationDto) {
        User user = new User();
        user.setUsername(userCreationDto.getUsername());
        user.setPassword(passwordEncoder.encode(userCreationDto.getPassword()));
        user.setEmail(userCreationDto.getEmail());
        user.setPhone(userCreationDto.getPhone());

        return user;
    }

    public User fromUserUpdateDtoToUser(UserUpdateDto userUpdateDto) {
        User user = new User();
        user.setEmail(userUpdateDto.getEmail());
        user.setPhone(userUpdateDto.getPhone());
        user.setFirstName(userUpdateDto.getFirstName());
        user.setLastName(userUpdateDto.getLastName());

        return user;
    }

    public UserUpdateDto fromUserToUserUpdateDto(User user) {
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setEmail(user.getEmail());
        updateDto.setPhone(user.getPhone());
        updateDto.setFirstName(user.getFirstName());
        updateDto.setLastName(user.getLastName());
        updateDto.setPicture(user.getPicture());
        return updateDto;
    }

    public TopUp fromTopUpDtoToTopUp(TopUpDto topUpDto, String key) {
        TopUp newTopUp = new TopUp();
        newTopUp.setAmount(topUpDto.getAmount());
        newTopUp.setCard(cardService.getCardById(topUpDto.getCardId()));
        newTopUp.setWallet(walletService.getWalletById(topUpDto.getWalletId()));
        newTopUp.setDate(new Date());
        newTopUp.setDescription(topUpDto.getDescription());
        newTopUp.setIdempotencyKey(key);

        return newTopUp;
    }

    public Wallet fromWalletDtoToWallet(WalletDto walletDto, String username) {
        Wallet wallet = new Wallet();
        wallet.setName(walletDto.getName());
        wallet.setUser(userService.getUserByUsername(username));
        return wallet;
    }

    public WalletDto fromWalletToWalletDto(Wallet wallet, String username) {
        WalletDto walletDto = new WalletDto();
        walletDto.setId(wallet.getId());
        walletDto.setName(wallet.getName());
        walletDto.setCreatorName(userService.getUserByUsername(username).getUsername());
        return walletDto;
    }

    public Transaction fromTransactionCreationDtoToTransaction(TransactionCreationDto transactionCreationDto,
                                                               int fromWallet, String fromUser) {
        Transaction transaction = new Transaction();
        transaction.setFromWallet(walletService.getWalletById(fromWallet));
        transactionService.setToUserField(transaction, transactionCreationDto.getToUser());
        transaction.setFromUser(fromUser);
        transaction.setDescription(transactionCreationDto.getDescription());
        transaction.setAmount(transactionCreationDto.getAmount());
        transaction.setCategory(categoryService.getById(transactionCreationDto.getCategoryId()));

        return transaction;
    }

    public Transaction fromTransactionCreationDtoToTransaction(TransactionCreationDto transactionCreationDto,
                                                               String fromUser) {
        Transaction transaction = new Transaction();
        transaction.setFromWallet(walletService.getWalletById(transactionCreationDto.getFromWalletId()));
        transactionService.setToUserField(transaction, transactionCreationDto.getToUser());
        transaction.setFromUser(fromUser);
        transaction.setDescription(transactionCreationDto.getDescription());
        transaction.setAmount(transactionCreationDto.getAmount());
        transaction.setCategory(categoryService.getById(transactionCreationDto.getCategoryId()));

        return transaction;
    }

    public Page<UserDisplayDto> fromUserToUserDisplayDto(List<User> userList, Pageable pageable) {
        List<UserDisplayDto> userDisplayDtoList = new ArrayList<>();
        for (User user : userList) {
            UserDisplayDto userDisplayDto = new UserDisplayDto();
            userDisplayDto.setUsername(user.getUsername());
            userDisplayDto.setPicture(user.getPicture());
            userDisplayDtoList.add(userDisplayDto);
        }

        return PageHelper.pagedList(userDisplayDtoList, pageable);
    }

    public CategoryDto fromCategoryToCategoryDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(category.getName());
        return categoryDto;
    }
}