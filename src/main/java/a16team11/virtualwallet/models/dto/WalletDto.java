package a16team11.virtualwallet.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class WalletDto {

    private int id;

    @NotBlank
    @NotNull
    private String name;

    private String creatorName;

    public WalletDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }
}
