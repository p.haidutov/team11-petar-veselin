package a16team11.virtualwallet.exception.exceptionhandlers;

import a16team11.virtualwallet.exception.DuplicateEntityException;
import a16team11.virtualwallet.exception.EntityNotFoundException;
import a16team11.virtualwallet.exception.InsufficientFundsException;
import a16team11.virtualwallet.exception.UnauthorizedException;
import a16team11.virtualwallet.models.ExceptionResponseBody;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestControllerAdvice(annotations = RestController.class)
@Order(1)
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {InsufficientFundsException.class, DuplicateEntityException.class})
    protected ResponseEntity<Object> handleUnprocessableEntity(RuntimeException ex, WebRequest request) {
        ExceptionResponseBody body = new ExceptionResponseBody(
                HttpStatus.UNPROCESSABLE_ENTITY.value(),
                HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase(),
                ex.getMessage());

        return handleExceptionInternal(ex, body, new HttpHeaders(),
                HttpStatus.UNPROCESSABLE_ENTITY, request);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(EntityNotFoundException ex, WebRequest request) {
        ExceptionResponseBody body = new ExceptionResponseBody(
                HttpStatus.NOT_FOUND.value(),
                HttpStatus.NOT_FOUND.getReasonPhrase(),
                ex.getMessage());

        return handleExceptionInternal(ex, body, new HttpHeaders(),
                HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {UnauthorizedException.class})
    protected ResponseEntity<Object> handleUnauthorized(UnauthorizedException ex, WebRequest request) {
        ExceptionResponseBody body = new ExceptionResponseBody(
                HttpStatus.FORBIDDEN.value(),
                HttpStatus.FORBIDDEN.getReasonPhrase(),
                ex.getMessage());

        return handleExceptionInternal(ex, body, new HttpHeaders(),
                HttpStatus.FORBIDDEN, request);

    }

    @ExceptionHandler(value = {HttpClientErrorException.BadRequest.class, ConstraintViolationException.class})
    protected ResponseEntity<Object> handleForbidden(RuntimeException ex, WebRequest request) {
        ExceptionResponseBody body = new ExceptionResponseBody(
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(),
                ex.getMessage());

        return handleExceptionInternal(ex, body, new HttpHeaders(),
                HttpStatus.FORBIDDEN, request);
    }

    //Exception thrown by Bank API
    @ExceptionHandler(value = {HttpClientErrorException.Forbidden.class})
    protected ResponseEntity<Object> handleForbidden(HttpClientErrorException.Forbidden ex, WebRequest request) {
        ExceptionResponseBody body = new ExceptionResponseBody(
                HttpStatus.FORBIDDEN.value(),
                HttpStatus.FORBIDDEN.getReasonPhrase(),
                ex.getMessage());

        return handleExceptionInternal(ex, body, new HttpHeaders(),
                HttpStatus.FORBIDDEN, request);
    }

//    //Exception thrown by Bank API
//    @ExceptionHandler(value = {HttpClientErrorException.Unauthorized.class})
//    protected ResponseEntity<Object> handleHttpUnauthorized(RuntimeException ex, WebRequest request) {
//        ExceptionResponseBody body = new ExceptionResponseBody(
//                HttpStatus.UNAUTHORIZED.value(),
//                HttpStatus.UNAUTHORIZED.getReasonPhrase(),
//                ex.getMessage());
//
//        return handleExceptionInternal(ex, body, new HttpHeaders(),
//                HttpStatus.UNAUTHORIZED, request);
//    }
//
//    //Exception thrown by Bank API
//    @ExceptionHandler(value = {HttpClientErrorException.BadRequest.class})
//    protected ResponseEntity<Object> handleHttpBadRequest(RuntimeException ex, WebRequest request) {
//        ExceptionResponseBody body = new ExceptionResponseBody(
//                HttpStatus.BAD_REQUEST.value(),
//                HttpStatus.BAD_REQUEST.getReasonPhrase(),
//                ex.getMessage());
//
//        return handleExceptionInternal(ex, body, new HttpHeaders(),
//                HttpStatus.BAD_REQUEST, request);
//    }

    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        //Get all errors
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        body.put("errors", errors);

        return new ResponseEntity<>(body, headers, status);

    }
}
