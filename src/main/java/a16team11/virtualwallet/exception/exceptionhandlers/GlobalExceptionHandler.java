package a16team11.virtualwallet.exception.exceptionhandlers;

import a16team11.virtualwallet.exception.*;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;

import static a16team11.virtualwallet.utils.Constants.*;

@ControllerAdvice(annotations = Controller.class)
public class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    protected ModelAndView handleEntityNotFoundException(EntityNotFoundException ex) {
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.NOT_FOUND);
        model.addObject(STATUS, HttpStatus.NOT_FOUND.value());
        model.addObject(ERROR_MSG, ex.getMessage());

        return model;
    }

    @ExceptionHandler(value = {InsufficientFundsException.class, DuplicateEntityException.class})
    protected ModelAndView handleUnprocessableEntity(RuntimeException ex) {
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);
        model.addObject(STATUS, HttpStatus.UNPROCESSABLE_ENTITY.value());
        model.addObject(ERROR_MSG, ex.getMessage());

        return model;
    }

    @ExceptionHandler(value = {UnauthorizedException.class})
    protected ModelAndView handleUnprocessableEntity(UnauthorizedException ex) {
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);
        model.addObject(STATUS, HttpStatus.UNPROCESSABLE_ENTITY.value());
        model.addObject(ERROR_MSG, ex.getMessage());

        return model;
    }

    //Thrown by bank api
    @ExceptionHandler(value = {HttpClientErrorException.Forbidden.class})
    protected ModelAndView handleForbidden(HttpClientErrorException.Forbidden ex) {
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);
        model.addObject(STATUS, HttpStatus.FORBIDDEN.value());
        model.addObject(ERROR_MSG, INSUFFICIENT_FUNDS_TO_COMPLETE_THE_TRANSACTION);

        return model;
    }

    @ExceptionHandler(value = {HttpClientErrorException.BadRequest.class, ConstraintViolationException.class,
            BindException.class})
    protected ModelAndView handleForbidden(RuntimeException ex) {
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.BAD_REQUEST);
        model.addObject(STATUS, HttpStatus.BAD_REQUEST.value());
        model.addObject(ERROR_MSG, OOPS_INVALID_REQUEST);

        return model;
    }

    @ExceptionHandler(value = {FileSizeLimitExceededException.class, MaxUploadSizeExceededException.class})
    protected ModelAndView handleFileSize(RuntimeException ex){
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.BAD_REQUEST);
        model.addObject(STATUS, HttpStatus.BAD_REQUEST.value());
        model.addObject(ERROR_MSG, FILE_SIZE_EXCEEDED_ERROR);

        return model;
    }

    @ExceptionHandler(value = {PasswordsDoNotMatchException.class})
    protected ModelAndView handlePasswordsNotMatching(PasswordsDoNotMatchException ex){
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.BAD_REQUEST);
        model.addObject(STATUS, HttpStatus.BAD_REQUEST.value());
        model.addObject(ERROR_MSG, ex.getMessage());

        return model;
    }


    @ExceptionHandler(value = {NoHandlerFoundException.class})
    protected ModelAndView handle404(NoHandlerFoundException ex) {
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.NOT_FOUND);
        model.addObject(STATUS, HttpStatus.NOT_FOUND.value());
        model.addObject(ERROR_MSG, OOPS_INVALID_PAGE_ERROR);

        return model;
    }

    //Generic
    @ExceptionHandler(value = {Exception.class})
    protected ModelAndView handleGenericException(Exception ex) {
        ModelAndView model = new ModelAndView(ERROR_STRING);
        model.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        model.addObject(ERROR_MSG, OOPS_SOMETHING_WENT_WRONG);

        return model;
    }
}
