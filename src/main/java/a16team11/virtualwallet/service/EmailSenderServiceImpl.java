package a16team11.virtualwallet.service;

import a16team11.virtualwallet.models.Token;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.service.common.EmailSenderService;
import a16team11.virtualwallet.service.common.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static a16team11.virtualwallet.utils.Constants.*;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {


    private JavaMailSender javaMailSender;
    private TokenService tokenService;

    @Autowired
    public EmailSenderServiceImpl(JavaMailSender javaMailSender, TokenService tokenService) {
        this.javaMailSender = javaMailSender;
        this.tokenService = tokenService;
    }

    @Async
    public void sendEmail(User user) {
        Token token = new Token(user);
        tokenService.save(token);
        SimpleMailMessage mailToSend = getMailMessage(user, token);
        javaMailSender.send(mailToSend);
    }

    private SimpleMailMessage getMailMessage(User user, Token token){
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setTo(user.getEmail());
        mailMessage.setFrom(EMAIL_SENDER);
        mailMessage.setSubject(CONFIRM_ACCOUNT_TITLE);

        String emailMessage = CONFIRM_ACCOUNT_MESSAGE + URL + token.getToken();
        mailMessage.setText(emailMessage);

        return  mailMessage;
    }
}