package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.EntityNotFoundException;
import a16team11.virtualwallet.models.Token;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.TokenRepository;
import a16team11.virtualwallet.service.common.TokenService;
import a16team11.virtualwallet.service.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static a16team11.virtualwallet.utils.Constants.INVALID_TOKEN_MESSAGE;

@Service
public class TokenServiceImpl implements TokenService {

    private TokenRepository tokenRepository;
    private UserService userService;

    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository, UserService userService) {
        this.tokenRepository = tokenRepository;
        this.userService = userService;
    }

    @Override
    public Token find(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    public void save(Token token) {
        tokenRepository.save(token);
    }

    @Override
    public void delete(Token token) {
        tokenRepository.delete(token);
    }

    @Override
    public void confirmToken(String confirmationToken) {
        Token token = tokenRepository.findByToken(confirmationToken);
        if (token != null) {
            User user = token.getUser();
            User result = userService.findByEmailIgnoreCase(user.getEmail());
            result.setEnabled(true);
            userService.save(result);
        } else {
            throw new EntityNotFoundException(INVALID_TOKEN_MESSAGE);
        }
    }
}
