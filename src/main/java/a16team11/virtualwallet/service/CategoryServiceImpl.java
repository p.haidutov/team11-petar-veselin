package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.DuplicateEntityException;
import a16team11.virtualwallet.exception.EntityNotFoundException;
import a16team11.virtualwallet.exception.UnauthorizedException;
import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.CategoryRepository;
import a16team11.virtualwallet.service.common.CategoryService;
import a16team11.virtualwallet.service.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static a16team11.virtualwallet.utils.Constants.*;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;
    private UserService userService;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository,
                               UserService userService) {
        this.categoryRepository = categoryRepository;
        this.userService = userService;
    }

    @Override
    public List<Category> getAllByUsername(String username) {
        return categoryRepository.getAllByUsernameAndEnabledTrue(username);
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getByIdAndEnabledTrue(id);
    }

    @Override
    public void create(String name, String username) {
        throwExceptionIfUserAlreadyHasCategory(name, username);
        Category category = new Category(name, username);
        categoryRepository.save(category);
    }

    @Override
    public void createGeneral(String username) {
        Category category = new Category(GENERAL_CATEGORY, username);
        categoryRepository.save(category);
    }

    @Override
    public void update(int id, String name, String username) {
        User user = userService.getUserByUsername(username);
        Category category = categoryRepository.getByIdAndUsername(id, username);

        throwExceptionIfNoSuchCategory(category);
        throwExceptionIfUserAlreadyHasCategory(name, username);
        throwExceptionIfUserNotAuthorized(category, user);

        category.setName(name);
        categoryRepository.save(category);
    }

    @Override
    public void delete(int id, String username) {
        User user = userService.getUserByUsername(username);
        Category category = categoryRepository.getByIdAndUsername(id, username);

        throwExceptionIfNoSuchCategory(category);
        throwExceptionIfUserNotAuthorized(category, user);

        category.setEnabled(false);
        categoryRepository.save(category);
    }

    private void throwExceptionIfUserAlreadyHasCategory(String name, String username) {
        if (categoryRepository.existsByNameAndUsername(name, username)) {
            throw new DuplicateEntityException(DUPLICATE_CATEGORY);
        }
    }

    private void throwExceptionIfNoSuchCategory(Category category) {
        if (category == null) {
            throw new EntityNotFoundException(NO_SUCH_CATEGORY_ERROR);
        }
    }

    private void throwExceptionIfUserNotAuthorized(Category category, User user) {
        if (!category.getUsername().equals(user.getUsername())) {
            throw new UnauthorizedException(USER_NOT_AUTHORIZED_TO_ACCESS_THIS_RESOURCE);
        }
    }
}
