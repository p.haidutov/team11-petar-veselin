package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.DuplicateEntityException;
import a16team11.virtualwallet.exception.InsufficientFundsException;
import a16team11.virtualwallet.exception.UnauthorizedException;
import a16team11.virtualwallet.models.*;
import a16team11.virtualwallet.repository.TransactionRepository;
import a16team11.virtualwallet.service.common.*;
import a16team11.virtualwallet.utils.FieldUpdater;
import a16team11.virtualwallet.utils.RegexChecks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static a16team11.virtualwallet.utils.Constants.*;

@Service
public class TransactionServiceImpl implements TransactionService {
    private TransactionRepository transactionRepository;
    private UserService userService;
    private WalletService walletService;
    private TopUpService topUpService;
    private FieldUpdater fieldUpdater;
    private CategoryService categoryService;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  UserService userService,
                                  WalletService walletService,
                                  TopUpService topUpService,
                                  FieldUpdater fieldUpdater,
                                  CategoryService categoryService) {
        this.transactionRepository = transactionRepository;
        this.userService = userService;
        this.walletService = walletService;
        this.fieldUpdater = fieldUpdater;
        this.categoryService = categoryService;
        this.topUpService = topUpService;
    }

    @Override
    public void checkIdempotencyKey(String idempotencyKey) {
        if (transactionRepository.existsByIdempotencyKey(idempotencyKey)) {
            throw new DuplicateEntityException(TRANSACTION_ALREADY_PROCESSED_ERROR);
        }
    }

    @Override
    public Page<Transaction> getAll(String username, Pageable pageable) {
        userService.checkIfUserIsAdmin(username);
        return transactionRepository.getAllByOrderByDateDesc(pageable);
    }

    @Override
    public List<Transaction> getTop5ByUsername(String username) {
        return transactionRepository.getTop5ByFromUserOrToUserOrderByDateDesc(username, username);
    }

    @Override
    public Page<MixedTransaction> getMixedTransactionsByUsername(String fromUsername, String toUsername,
                                                                 int categoryId, String startDate,
                                                                 String endDate, String sortBy,
                                                                 Pageable pageable) throws Exception {
        Date dateStart = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
        Date dateEnd = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);


        User user = userService.getUserByUsername(fromUsername);
        List<Transaction> transactionList = new ArrayList<>();
        List<TopUp> topUpList = new ArrayList<>();
        if (categoryId == 0 && toUsername.isEmpty()) {
            transactionList =
                    transactionRepository.getTransactionByFromUserOrToUserAndToUserContainingAndDateBetween(
                            fromUsername, fromUsername, toUsername, dateStart, dateEnd);

            topUpList = topUpService.getAllByUsernameAndDate(user, dateStart, dateEnd);
        } else if (categoryId == ALL_TRANSACTIONS) {
            transactionList =
                    transactionRepository.getTransactionByFromUserAndToUserContainingAndDateBetween(
                            fromUsername, toUsername, dateStart, dateEnd);
        } else if (categoryId == TOP_UPS_ONLY) {
            topUpList = topUpService.getAllByUsernameAndDate(user, dateStart, dateEnd);
        } else {
            Category category = categoryService.getById(categoryId);

            transactionList =
                    transactionRepository.getTransactionByFromUserAndToUserContainingAndCategoryAndDateBetween(
                            fromUsername, toUsername, category, dateStart, dateEnd);
        }
        List<MixedTransaction> mixedTransactions = new ArrayList<>();

        fillMixedTransactionListWithTransactions(mixedTransactions, transactionList, fromUsername);
        fillMixedTransactionListWithTopUps(mixedTransactions, topUpList);

        mixedTransactions = sortMixedTransactionList(mixedTransactions, sortBy);

        return PageHelper.pagedList(mixedTransactions, pageable);
    }

    @Override
    //Admin method
    public Page<MixedTransaction> getAllMixedTransactions(String adminName, String fromUsername,
                                                          String toUsername, String startDate,
                                                          String endDate, String sortBy,
                                                          Pageable pageable) throws Exception {
        Date dateStart = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
        Date dateEnd = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);

        userService.checkIfUserIsAdmin(adminName);
        User fromUser = null;
        User toUser = null;
        if (!fromUsername.isEmpty()) {
            fromUser = userService.getUserByUsername(fromUsername);
        }
        if (!toUsername.isEmpty()) {
            toUser = userService.getUserByUsername(toUsername);
        }

        List<Transaction> transactionList =
                transactionRepository.getTransactionByFromUserContainingAndToUserContainingAndDateBetween(
                        fromUsername, toUsername, dateStart, dateEnd);

        List<TopUp> topUpList;

        if (fromUser != null && toUser != null && fromUser.getUsername().equals(toUser.getUsername())) {
            topUpList = topUpService.getAllByUsernameOrUsernameAndDate(fromUser, toUser, dateStart, dateEnd);
        } else if (fromUser != null && toUser != null) {
            topUpList = Collections.emptyList();
        } else if (fromUser != null) {
            topUpList = topUpService.getAllByUsernameAndDate(fromUser, dateStart, dateEnd);
        } else if (toUser != null) {
            topUpList = topUpService.getAllByUsernameAndDate(toUser, dateStart, dateEnd);
        } else {
            topUpList = topUpService.getAllByDate(dateStart, dateEnd);
        }

        List<MixedTransaction> mixedTransactions = new ArrayList<>();

        for (Transaction transaction : transactionList) {
            MixedTransaction mixedTransaction = new MixedTransaction();
            mixedTransaction.setAmount(transaction.getAmount());
            mixedTransaction.setDate(transaction.getDate());
            mixedTransaction.setFrom(transaction.getFromUser() + " - " + transaction.getFromWallet().getName());
            mixedTransaction.setTo(transaction.getToUser() + " - " + transaction.getToWallet().getName());

            mixedTransactions.add(mixedTransaction);
        }

        for (TopUp topUp : topUpList) {
            MixedTransaction mixedTransaction = new MixedTransaction();
            mixedTransaction.setAmount(topUp.getAmount());
            mixedTransaction.setDate(topUp.getDate());      //gets the last 4 digits
            mixedTransaction.setFrom(topUp.getUser().getUsername() + " - *" + topUp.getCard().getNumber().substring(15));
            mixedTransaction.setTo(topUp.getUser().getUsername() + " - " + topUp.getWallet().getName());

            mixedTransactions.add(mixedTransaction);
        }

        mixedTransactions = sortMixedTransactionList(mixedTransactions, sortBy);

        return PageHelper.pagedList(mixedTransactions, pageable);
    }

    @Override
    public List<MixedTransaction> getTop5MixedTransactions(String username) {
        User user = userService.getUserByUsername(username);
        List<Transaction> transactionList = getTop5ByUsername(username);
        List<TopUp> topUpList = topUpService.getTop5ByUsername(user);

        List<MixedTransaction> mixedTransactions = new ArrayList<>();

        fillMixedTransactionListWithTransactions(mixedTransactions, transactionList, username);
        fillMixedTransactionListWithTopUps(mixedTransactions, topUpList);

        return mixedTransactions.stream()
                .sorted(Comparator.comparing(MixedTransaction::getDate).reversed())
                .limit(5)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Transaction createTransaction(Transaction transaction, String key, String fromUsername, int fromWalletId) {
        checkIdempotencyKey(key);

        User fromUser = userService.getUserByUsername(transaction.getFromUser());
        User toUser = userService.getUserByUsername(transaction.getToUser());
        Wallet toWallet = walletService.getPrimaryWallet(toUser);

        fieldUpdater.updateTransactionFields(transaction, key, toWallet);

        if (fromUser.isBlocked() || toUser.isBlocked()) {
            throw new UnauthorizedException(USER_IS_BLOCKED_ERROR);
        }

        if (fromUser != transaction.getFromWallet().getUser() || toUser != transaction.getToWallet().getUser()) {
            throw new UnauthorizedException(WALLET_DOES_NOT_BELONG_TO_USER_ERROR);
        }

        if (transaction.getFromWallet().getBalance().compareTo(transaction.getAmount()) < 0) {
            throw new InsufficientFundsException(INSUFFICIENT_FUNDS_IN_WALLET_ERROR);
        }
        if(transaction.getCategory() == null){
            throw new UnauthorizedException(INVALID_CATEGORY_TRANSACTION_ERROR);
        }

        walletService.changeBalance(transaction.getFromWallet(), transaction.getAmount().negate());
        walletService.changeBalance(transaction.getToWallet(), transaction.getAmount());

        transactionRepository.save(transaction);

        return transactionRepository.getByIdempotencyKey(transaction.getIdempotencyKey());
    }

    @Override
    public void setToUserField(Transaction transaction, String userField) {
        User user;
        if (RegexChecks.checkIfStringIsValidPhone(userField)) {
            user = userService.getUserByPhone(userField);
            transaction.setToUser(user.getUsername());
        } else if (RegexChecks.checkIfStringIsValidEmail(userField)) {
            user = userService.getUserByEmail(userField);
            transaction.setToUser(user.getUsername());
        } else {
            user = userService.getUserByUsername(userField);
            transaction.setToUser(user.getUsername());
        }
    }

    private List<MixedTransaction> sortMixedTransactionList(List<MixedTransaction> mixedTransactions, String sortBy) {
        switch (sortBy) {
            case "DateDesc":
                mixedTransactions = mixedTransactions.stream()
                        .sorted(Comparator.comparing(MixedTransaction::getDate).reversed())
                        .collect(Collectors.toList());
                break;
            case "DateAsc":
                mixedTransactions = mixedTransactions.stream()
                        .sorted(Comparator.comparing(MixedTransaction::getDate))
                        .collect(Collectors.toList());
                break;
            case "AmountAsc":
                mixedTransactions = mixedTransactions.stream()
                        .sorted(Comparator.comparing(MixedTransaction::getAmount))
                        .collect(Collectors.toList());
                break;
            case "AmountDesc":
                mixedTransactions = mixedTransactions.stream()
                        .sorted(Comparator.comparing(MixedTransaction::getAmount).reversed())
                        .collect(Collectors.toList());
                break;
        }
        return mixedTransactions;
    }

    private void fillMixedTransactionListWithTransactions(List<MixedTransaction> mixedTransactions,
                                                          List<Transaction> transactionList,
                                                          String username) {
        for (Transaction transaction : transactionList) {
            MixedTransaction mixedTransaction = new MixedTransaction();
            mixedTransaction.setAmount(transaction.getAmount());
            mixedTransaction.setDate(transaction.getDate());
            mixedTransaction.setCategoryName(transaction.getCategory().getName());
            mixedTransaction.setDescription(transaction.getDescription());

            if (transaction.getFromUser().equals(username)) {
                mixedTransaction.setFrom(transaction.getFromWallet().getName());
            } else {
                mixedTransaction.setFrom(transaction.getFromUser());
            }

            if (transaction.getToUser().equals(username)) {
                mixedTransaction.setTo(transaction.getToWallet().getName());
            } else {
                mixedTransaction.setTo(transaction.getToUser());
            }

            if (transaction.getToUser().equals(username)) {
                mixedTransaction.setType(INCOMING_TYPE);
            } else {
                mixedTransaction.setType(OUTGOING_TYPE);
            }

            mixedTransactions.add(mixedTransaction);
        }
    }

    private void fillMixedTransactionListWithTopUps(List<MixedTransaction> mixedTransactions,
                                                    List<TopUp> topUpList) {
        for (TopUp topUp : topUpList) {
            MixedTransaction mixedTransaction = new MixedTransaction();
            mixedTransaction.setAmount(topUp.getAmount());
            mixedTransaction.setDate(topUp.getDate());      //gets the last 4 digits
            mixedTransaction.setFrom("*" + topUp.getCard().getNumber().substring(15));
            mixedTransaction.setTo(topUp.getWallet().getName());
            mixedTransaction.setType(INCOMING_TYPE);
            mixedTransaction.setDescription(topUp.getDescription());
            mixedTransaction.setCategoryName(DEPOSIT_CATEGORY);

            mixedTransactions.add(mixedTransaction);
        }
    }
}