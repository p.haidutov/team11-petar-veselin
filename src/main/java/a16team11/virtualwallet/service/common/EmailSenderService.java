package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.User;

public interface EmailSenderService {

    void sendEmail(User user);
}