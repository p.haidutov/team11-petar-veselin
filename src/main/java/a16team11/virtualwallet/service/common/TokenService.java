package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.Token;

public interface TokenService {

    Token find(String token);

    void save(Token token);

    void delete(Token token);

    void confirmToken(String confirmationToken);
}
