package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserService {
    User createUser(User user);

    List<User> getAll();

    User getByField(int id, String username, String phone, String email);

    void update(int id, User user, String username, MultipartFile file) throws IOException;

    void delete(int id, String username);

    User getUserById(int id);

    User getUserByUsername(String username);

    User getUserByPhone(String phone);

    User getUserByEmail(String email);

    Page<User> getPageByFields(
            String username,
            String email,
            String phone,
            Pageable pageable);

    String getKey();

    List<String> getUserAuthorities(String username);

    void save(User user);

    User findByEmailIgnoreCase(String email);

    void checkIfUserIsAdmin(String username);

    void updatePassword(String oldPassword, String newPassword, String confirmNewPassword, String name);
}