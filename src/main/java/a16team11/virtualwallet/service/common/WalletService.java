package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;

import java.math.BigDecimal;
import java.util.List;

public interface WalletService {
    List<Wallet> getAllByUser(User user);

    Wallet getWalletByField(int id, String name, String username);

    Wallet getWalletById(int id);

    Wallet getWalletByName(String name);

    Wallet getPrimaryWallet(User user);

    Wallet create(Wallet wallet);

    Wallet createPrimary(User user);

    void changeBalance(Wallet wallet, BigDecimal amount);

    void update(String name, int id, String username);

    void delete(int id, String username);
}
