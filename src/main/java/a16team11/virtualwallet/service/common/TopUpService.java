package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.dto.TopUpDto;

import java.security.Principal;
import java.util.Date;
import java.util.List;

public interface TopUpService {
    List<TopUp> getAllByUsernameAndDate(User user, Date startDate, Date endDate);

    List<TopUp> getAllByUsernameOrUsernameAndDate(User user, User user2, Date startDate, Date endDate);

    List<TopUp> getAllByDate(Date startDate, Date endDate);

    List<TopUp> getTop5ByUsername(User user);

    void checkIdempotencyKey(String idempotencyKey);

    TopUp topUp(String username, TopUp topUp);

    void requestFundsFromBankApi(TopUp topUp, String key);
}
