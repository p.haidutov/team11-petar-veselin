package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.User;

import java.util.List;

public interface CardService {
    Card getCardByField(int id, String number, String username);

    Card getCardById(int id);

    Card getCardByNumber(String number);

    void create(Card card);

    void update(int id, Card card, String username);

    void delete(int id, String username);

    List<Card> getUserCards(User user);
}
