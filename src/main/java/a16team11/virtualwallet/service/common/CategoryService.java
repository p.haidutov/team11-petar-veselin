package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllByUsername(String username);

    Category getById(int id);

    void create(String name, String username);

    void createGeneral(String username);

    void update(int id, String name, String username);

    void delete(int id, String username);
}
