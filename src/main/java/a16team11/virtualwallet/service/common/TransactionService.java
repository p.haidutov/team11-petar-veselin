package a16team11.virtualwallet.service.common;

import a16team11.virtualwallet.models.MixedTransaction;
import a16team11.virtualwallet.models.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface TransactionService {
    void checkIdempotencyKey(String idempotencyKey);

    Page<Transaction> getAll(String username, Pageable pageable);

    List<Transaction> getTop5ByUsername(String username);

    Page<MixedTransaction> getMixedTransactionsByUsername(String fromUsername, String toUsername,
                                                          int categoryId, String startDate,
                                                          String endDate, String sortBy,
                                                          Pageable pageable) throws Exception;

    Page<MixedTransaction> getAllMixedTransactions(String adminName, String fromUsername,
                                                   String toUsername, String startDate,
                                                   String endDate, String sortBy, Pageable pageable) throws Exception;

    List<MixedTransaction> getTop5MixedTransactions(String username);

    Transaction createTransaction(Transaction transaction, String key, String fromUsername, int fromWalletId);

    void setToUserField(Transaction transaction, String userField);
}
