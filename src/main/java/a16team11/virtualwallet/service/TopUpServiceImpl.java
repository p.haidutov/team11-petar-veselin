package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.DuplicateEntityException;
import a16team11.virtualwallet.exception.UnauthorizedException;
import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.TopUpRepository;
import a16team11.virtualwallet.service.common.TopUpService;
import a16team11.virtualwallet.service.common.UserService;
import a16team11.virtualwallet.service.common.WalletService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;

import static a16team11.virtualwallet.utils.Constants.*;

@Service
public class TopUpServiceImpl implements TopUpService {
    private TopUpRepository topUpRepository;
    private UserService userService;
    private WalletService walletService;

    public TopUpServiceImpl(TopUpRepository topUpRepository,
                            UserService userService,
                            WalletService walletService) {
        this.topUpRepository = topUpRepository;
        this.userService = userService;
        this.walletService = walletService;
    }

    @Override
    public List<TopUp> getAllByUsernameAndDate(User user, Date startDate, Date endDate) {
        return topUpRepository.getAllByUserAndDateBetweenOrderByDate(user, startDate, endDate);
    }

    @Override
    public List<TopUp> getAllByUsernameOrUsernameAndDate(User user, User user2, Date startDate, Date endDate) {
        return topUpRepository.getAllByUserOrUserAndDateBetweenOrderByDate(user, user2, startDate, endDate);
    }

    @Override
    public List<TopUp> getAllByDate(Date startDate, Date endDate) {
        return topUpRepository.getAllByDateBetweenOrderByDate(startDate, endDate);
    }

    @Override
    public List<TopUp> getTop5ByUsername(User user) {
        return topUpRepository.getTop5ByUserOrderByDateDesc(user);
    }

    @Override
    @Transactional
    public TopUp topUp(String username, TopUp topUp) {
        requestFundsFromBankApi(topUp, topUp.getIdempotencyKey());

        User user = userService.getUserByUsername(username);
        if(user.isBlocked()){
            throw new UnauthorizedException(USER_IS_BLOCKED_ERROR);
        }
        topUp.setUser(user);
        walletService.changeBalance(topUp.getWallet(), topUp.getAmount());
        topUpRepository.save(topUp);
        return topUpRepository.getByIdempotencyKey(topUp.getIdempotencyKey());
    }

    @Override
    public void requestFundsFromBankApi(TopUp topUp, String key) {
        checkIdempotencyKey(key);

        HttpHeaders header = new HttpHeaders();
        header.set(X_API_KEY, key);
        HttpEntity<TopUp> entity = new HttpEntity<>(topUp, header);
        RestTemplate rt = new RestTemplate();

        rt.postForEntity(PAYMENT_API_URI, entity, String.class);
    }

    @Override
    public void checkIdempotencyKey(String idempotencyKey) {
        if (topUpRepository.existsByIdempotencyKey(idempotencyKey)) {
            throw new DuplicateEntityException(TRANSACTION_ALREADY_PROCESSED_ERROR);
        }
    }
}
