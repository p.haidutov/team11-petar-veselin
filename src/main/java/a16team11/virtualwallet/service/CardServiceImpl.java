package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.DuplicateEntityException;
import a16team11.virtualwallet.exception.EntityNotFoundException;
import a16team11.virtualwallet.exception.UnauthorizedException;
import a16team11.virtualwallet.models.Card;

import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.CardRepository;
import a16team11.virtualwallet.service.common.CardService;
import a16team11.virtualwallet.service.common.UserService;
import a16team11.virtualwallet.utils.FieldUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static a16team11.virtualwallet.utils.Constants.*;

@Service
public class CardServiceImpl implements CardService {
    private CardRepository cardRepository;
    private FieldUpdater fieldUpdater;
    private UserService userService;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository, FieldUpdater fieldUpdater, UserService userService) {
        this.cardRepository = cardRepository;
        this.fieldUpdater = fieldUpdater;
        this.userService = userService;
    }

    @Override
    public Card getCardByField(int id, String number, String username) {
        Card card = null;
        if (id != 0) {
            card = getCardById(id);
        }
        if (!number.isEmpty()) {
            card = getCardByNumber(number);
        } else if (card == null) {
            throw new EntityNotFoundException(NO_SUCH_CARD_ERROR);
        }

        User user = userService.getUserByUsername(username);
        if(card.getUser() != user && !userService.getUserAuthorities(user.getUsername()).contains(ADMIN)){
            throw new UnauthorizedException(CARD_DOES_NOT_BELONG_TO_USER_ERROR);
        }

        return card;
    }

    @Override
    public Card getCardById(int id) {
        Card card = cardRepository.getCardByIdAndEnabledTrue(id);
        if (card == null) {
            throw new EntityNotFoundException(NO_SUCH_CARD_ERROR);
        }
        return card;
    }

    @Override
    public Card getCardByNumber(String number) {
        Card card = cardRepository.getCardByNumberAndEnabledTrue(number);
        if (card == null) {
            throw new EntityNotFoundException(NO_SUCH_CARD_ERROR);
        }
        return card;
    }

    @Override
    public void create(Card card) {

        String cardNumb = card.getNumber();
        cardNumb = cardNumb.replaceAll(" ", "");
        cardNumb = cardNumb.replaceAll("-", "");

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 1; i < cardNumb.length() + 1; i++) {
            strBuilder.append(cardNumb.charAt(i - 1));
            if (i % 4 == 0) {
                strBuilder.append(" ");
            }
        }
        card.setNumber(strBuilder.toString());

        if (cardRepository.existsCardByNumber(card.getNumber())) {
            throw new DuplicateEntityException(DUPLICATE_CARD_ERROR);
        }
        cardRepository.save(card);
    }

    @Override
    @Transactional
    public void update(int id, Card cardUpdate, String username) {
        Card card = getCardById(id);
        User user = userService.getUserByUsername(username);

        checkIfUserIsAuthorized(card, user);

        if (cardRepository.existsCardByNumber(cardUpdate.getNumber())) {
            if (getCardByNumber(cardUpdate.getNumber()).getId() != card.getId()) {
                throw new DuplicateEntityException(DUPLICATE_CARD_ERROR);
            }
        }

        card = fieldUpdater.updateCardFields(card, cardUpdate);
        cardRepository.save(card);
    }

    @Override
    @Transactional
    public void delete(int id, String username) {
        Card card = getCardById(id);
        User user = userService.getUserByUsername(username);

        checkIfUserIsAuthorized(card, user);

        card.setEnabled(false);
        cardRepository.save(card);
    }

    @Override
    public List<Card> getUserCards(User user) {
        return cardRepository.getAllByUserAndEnabledTrue(user);
    }

    private void checkIfUserIsAuthorized(Card card, User user) {
        if (card.getUser() != user && !userService.getUserAuthorities(user.getUsername()).contains(ADMIN)) {
            throw new UnauthorizedException(CARD_DOES_NOT_BELONG_TO_USER_ERROR);
        }

        if (user.isBlocked()) {
            throw new UnauthorizedException(USER_IS_BLOCKED_ERROR);
        }
    }
}
