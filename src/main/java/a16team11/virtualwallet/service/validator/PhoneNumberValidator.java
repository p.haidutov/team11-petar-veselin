package a16team11.virtualwallet.service.validator;

import a16team11.virtualwallet.service.validator.common.ValidPhoneNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static a16team11.virtualwallet.utils.Constants.PHONE_REGEX;

public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {
    private static final String PHONE_PATTERN = PHONE_REGEX;

    private Pattern pattern;
    private Matcher matcher;

    @Override
    public void initialize(ValidPhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String phone, ConstraintValidatorContext context) {
        return (validatePhoneNumber(phone));
    }

    private boolean validatePhoneNumber(String phone) {
        pattern = Pattern.compile(PHONE_PATTERN);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }
}
