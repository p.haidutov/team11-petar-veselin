package a16team11.virtualwallet.service.validator;

import a16team11.virtualwallet.service.validator.common.ValidCardNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static a16team11.virtualwallet.utils.Constants.CARD_REGEX;

public class CardNumberValidator implements ConstraintValidator<ValidCardNumber, String> {
    private static final String CARD_PATTERN = CARD_REGEX;

    private Pattern pattern;
    private Matcher matcher;

    @Override
    public void initialize(ValidCardNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String card, ConstraintValidatorContext context) {
        return (validateCardNumber(card));
    }

    private boolean validateCardNumber(String card) {
        card = card.replaceAll("-", "");
        card = card.replaceAll(" ", "");
        pattern = Pattern.compile(CARD_PATTERN);
        matcher = pattern.matcher(card);
        return matcher.matches();
    }
}
