package a16team11.virtualwallet.service.validator;

import a16team11.virtualwallet.models.dto.UserCreationDto;
import a16team11.virtualwallet.service.validator.common.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        UserCreationDto user = (UserCreationDto) obj;
        return user.getPassword().equals(user.getPasswordConfirmation());
    }
}
