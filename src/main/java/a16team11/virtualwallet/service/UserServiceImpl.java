package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.DuplicateEntityException;
import a16team11.virtualwallet.exception.EntityNotFoundException;
import a16team11.virtualwallet.exception.PasswordsDoNotMatchException;
import a16team11.virtualwallet.exception.UnauthorizedException;
import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.UserRepository;
import a16team11.virtualwallet.service.common.*;
import a16team11.virtualwallet.utils.FieldUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static a16team11.virtualwallet.utils.Constants.*;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private WalletService walletService;
    private CategoryService categoryService;
    private CardService cardService;
    private FieldUpdater fieldUpdater;
    private PasswordEncoder passwordEncoder;
    private UserDetailsManager userDetailsManager;
    private EmailSenderService emailSenderService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           @Lazy WalletService walletService,
                           @Lazy CategoryService categoryService,
                           @Lazy CardService cardService,
                           FieldUpdater fieldUpdater,
                           PasswordEncoder passwordEncoder,
                           UserDetailsManager userDetailsManager,
                           @Lazy EmailSenderService emailSenderService) {
        this.userRepository = userRepository;
        this.walletService = walletService;
        this.categoryService = categoryService;
        this.fieldUpdater = fieldUpdater;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;
        this.emailSenderService = emailSenderService;
        this.cardService = cardService;
    }

    @Override
    public User createUser(User user) {
        checkIfUserCreationPossible(user);
        userRepository.save(user);
        userRepository.updateAuthorities(user.getUsername());
        walletService.createPrimary(user);
        categoryService.createGeneral(user.getUsername());
        emailSenderService.sendEmail(user);

        return userRepository.getUserByUsernameAndEnabledTrue(user.getUsername());
    }

    @Override
    public User getByField(int id, String username, String phone, String email) {
        User user = null;
        if (id != 0) {
            user = getUserById(id);
        } else if (!username.isEmpty()) {
            user = getUserByUsername(username);
        } else if (!phone.isEmpty()) {
            user = getUserByPhone(phone);
        } else if (!email.isEmpty()) {
            user = getUserByEmail(email);
        }
        if (user == null) {
            throw new EntityNotFoundException(NO_SUCH_USER_ERROR);
        }
        return user;
    }

    @Override
    public User getUserById(int id) {
        User user = userRepository.getUserByIdAndEnabledTrue(id);
        if (user == null) {
            throw new EntityNotFoundException(NO_SUCH_USER_ERROR);
        }
        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        User user = userRepository.getUserByUsernameAndEnabledTrue(username);
        if (user == null) {
            throw new EntityNotFoundException(NO_SUCH_USER_ERROR);
        }
        return user;
    }

    @Override
    public User getUserByPhone(String phone) {
        User user = userRepository.getUserByPhoneAndEnabledTrue(phone);
        if (user == null) {
            throw new EntityNotFoundException(NO_SUCH_USER_ERROR);
        }
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        User user = userRepository.getUserByEmailAndEnabledTrue(email);
        if (user == null) {
            throw new EntityNotFoundException(NO_SUCH_USER_ERROR);
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        List<User> users = userRepository.findAllByEnabledTrue();
        if (users.isEmpty()) {
            throw new EntityNotFoundException(EMPTY_USERS_LIST_ERROR);
        }

        return users;
    }

    @Override
    @Transactional
    public void update(int id, User userUpdate, String username, MultipartFile file) throws IOException {
        User user = getUserById(id);
        checkIfUserIsAuthorized(username, user);
        if (userRepository.existsUserByEmail(userUpdate.getEmail())) {
            if (userRepository.getUserByEmailAndEnabledTrue(userUpdate.getEmail()).getId() != user.getId()) {
                throw new DuplicateEntityException(EMAIL_ALREADY_USED_ERROR);
            }
        }
        if (userRepository.existsUserByPhoneAndEnabledTrue(userUpdate.getPhone())) {
            if (userRepository.getUserByPhoneAndEnabledTrue(userUpdate.getPhone()).getId() != user.getId()) {
                throw new DuplicateEntityException(PHONE_ALREADY_USED_ERROR);
            }
        }

        user = fieldUpdater.updateUserFields(user, userUpdate, file);
        userRepository.save(user);
    }

    @Override
    public void updatePassword(String oldPassword, String newPassword, String confirmNewPassword, String username) {
        if (!newPassword.equals(confirmNewPassword)) {
            throw new PasswordsDoNotMatchException(PASSWORDS_DO_NOT_MATCH_ERROR);
        }

        if (!passwordEncoder.matches(oldPassword, getUserByUsername(username).getPassword())) {
            throw new PasswordsDoNotMatchException(INVALID_OLD_PASSWORD_ERROR);
        }

        newPassword = passwordEncoder.encode(newPassword);
        userDetailsManager.changePassword(oldPassword, newPassword);
    }

    @Override
    @Transactional
    public void delete(int id, String username) {
        User user = getUserById(id);
        checkIfUserIsAuthorized(username, user);

        List<Card> listOfCards = cardService.getUserCards(user);
        for (Card card: listOfCards) {
            cardService.delete(card.getId(), user.getUsername());
        }

        user.setEnabled(false);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public String getKey() {
        return UUID.randomUUID().toString();
    }

    @Override
    public Page<User> getPageByFields(String username,
                                      String email,
                                      String phone,
                                      Pageable pageable) {
        return userRepository.getUserByUsernameContainingAndEmailContainingAndPhoneContainingAndEnabledTrue(
                username, email, phone, pageable);
    }

    @Override
    public List<String> getUserAuthorities(String username) {
        return userRepository.getUserAuthorities(username);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public User findByEmailIgnoreCase(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }

    @Override
    public void checkIfUserIsAdmin(String username) {
        if (!getUserAuthorities(username).contains(ADMIN)) {
            throw new UnauthorizedException(USER_NOT_AUTHORIZED_TO_ACCESS_THIS_RESOURCE);
        }
    }

    private void checkIfUserIsAuthorized(String username, User user) {
        if (!getUserAuthorities(username).contains(ADMIN)) {
            if (getUserByUsername(username) != user) {
                throw new UnauthorizedException(USER_NOT_AUTHORIZED_TO_ACCESS_THIS_RESOURCE);
            }
        }
    }

    private void checkIfUserCreationPossible(User user) {
        if (userRepository.existsUserByUsername(user.getUsername())) {
            throw new DuplicateEntityException(USERNAME_ALREADY_USED_ERROR);
        }
        if (userRepository.existsUserByEmail(user.getEmail())) {
            throw new DuplicateEntityException(EMAIL_ALREADY_USED_ERROR);
        }
        if (userRepository.existsUserByPhoneAndEnabledTrue(user.getPhone())) {
            throw new DuplicateEntityException(PHONE_ALREADY_USED_ERROR);
        }
    }
}