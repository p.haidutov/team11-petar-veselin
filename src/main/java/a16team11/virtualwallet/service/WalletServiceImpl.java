package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.DuplicateEntityException;
import a16team11.virtualwallet.exception.EntityNotFoundException;
import a16team11.virtualwallet.exception.UnauthorizedException;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.models.WalletType;
import a16team11.virtualwallet.repository.WalletRepository;
import a16team11.virtualwallet.repository.WalletTypeRepository;
import a16team11.virtualwallet.service.common.UserService;
import a16team11.virtualwallet.service.common.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static a16team11.virtualwallet.utils.Constants.*;

@Service
public class WalletServiceImpl implements WalletService {
    private WalletRepository walletRepository;
    private WalletTypeRepository walletTypeRepository;
    private UserService userService;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository,
                             WalletTypeRepository walletTypeRepository,
                             UserService userService) {
        this.walletRepository = walletRepository;
        this.walletTypeRepository = walletTypeRepository;
        this.userService = userService;
    }

    @Override
    public List<Wallet> getAllByUser(User user) {
        return walletRepository.getAllByUserAndEnabledTrue(user);
    }

    @Override
    public Wallet getWalletByField(int id, String name, String username) {
        Wallet wallet = null;
        if (id != 0) {
            wallet = getWalletById(id);
        } else if (!name.isEmpty()) {
            wallet = getWalletByName(name);
        }

        throwExceptionIfWalletDoesNotExist(wallet);
        checkIfUserIsAuthorized(username, wallet);

        return wallet;
    }

    @Override
    public Wallet getWalletById(int id) {
        Wallet wallet = walletRepository.getWalletByIdAndEnabledTrue(id);
        throwExceptionIfWalletDoesNotExist(wallet);
        return wallet;
    }

    @Override
    public Wallet getWalletByName(String name) {
        Wallet wallet = walletRepository.getWalletByNameAndEnabledTrue(name);
        throwExceptionIfWalletDoesNotExist(wallet);
        return wallet;
    }

    @Override
    public Wallet getPrimaryWallet(User user){
        WalletType walletType = walletTypeRepository.getByType(PRIMARY);
        Wallet wallet = walletRepository.getWalletByUserAndWalletType(user, walletType);

        throwExceptionIfWalletDoesNotExist(wallet);

        return wallet;
    }

    @Override
    public Wallet create(Wallet wallet) {
        checkIfUserAlreadyHasSuchWallet(wallet.getName(), wallet.getUser());
        wallet.setWalletType(walletTypeRepository.getByType(SECONDARY));
        return walletRepository.save(wallet);
    }

    @Override
    public Wallet createPrimary(User user) {
        Wallet wallet = new Wallet();
        wallet.setUser(user);
        wallet.setName(PRIMARY_WALLET_NAME);
        wallet.setWalletType(walletTypeRepository.getByType(PRIMARY));
        return walletRepository.save(wallet);
    }

    @Override
    @Transactional
    public void changeBalance(Wallet wallet, BigDecimal amount) {
        checkIfWalletIsEnabled(wallet);
        wallet.addBalance(amount);
        walletRepository.save(wallet);
    }

    @Override
    @Transactional
    public void update(String name, int id, String username) {
        Wallet wallet = getWalletById(id);
        User user = userService.getUserByUsername(username);

        checkIfUserAlreadyHasSuchWallet(name, user);
        if (name != null && !name.trim().isEmpty()) {
            wallet.setName(name);
        }
        checkIfUserIsAuthorized(username, wallet);

        walletRepository.save(wallet);
    }

    @Override
    @Transactional
    public void delete(int id, String username) {
        Wallet wallet = getWalletById(id);
        checkIfUserIsAuthorized(username, wallet);
        wallet.setEnabled(false);
        walletRepository.save(wallet);
    }

    private void checkIfUserIsAuthorized(String username, Wallet wallet) {
        User user = userService.getUserByUsername(username);
        if (wallet.getUser() != user && !userService.getUserAuthorities(username).contains(ADMIN)) {
            throw new UnauthorizedException(WALLET_DOES_NOT_BELONG_TO_USER_ERROR);
        }
    }

    private void checkIfWalletIsEnabled(Wallet wallet) {
        if (!wallet.isEnabled()) {
            throw new EntityNotFoundException(NO_SUCH_WALLET_ERROR);
        }
    }

    private void checkIfUserAlreadyHasSuchWallet(String name, User user) {
        if (walletRepository.existsWalletByNameAndUser(name, user)) {
            throw new DuplicateEntityException(USER_ALREADY_HAS_SUCH_WALLET_ERROR);
        }
    }

    private void throwExceptionIfWalletDoesNotExist(Wallet wallet){
        if (wallet == null) {
            throw new EntityNotFoundException(NO_SUCH_WALLET_ERROR);
        }
    }
}
