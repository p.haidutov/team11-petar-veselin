$(document).ready(function () {

    $('.col .eBtn').on('click', function (event) {

        event.preventDefault();
        var href = $(this).attr('href');
        $.get(href, function (user, status) {

            $('.myForm #id').val(user.id);
            $('.myForm #username').val(user.username);
            $('.myForm #email').val(user.email);
            $('.myForm #phone').val(user.phone);
            $('.myForm #blocked').val(user.blocked);
        });

        $('.myForm #exampleModal').modal();
    });

    $('.row .delBtn').on('click', function (event) {

        event.preventDefault();
        var href = $(this).attr('href');
        $('#staticBackdrop #delRef').attr('href', href);
        $('#staticBackdrop').modal();
    });
});

function fileValidation() {
    var fileInput = document.getElementById('file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    } else {
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}

function selectUser(text) {
    document.getElementById('emailID').value = text;
}

function validateCard()
{
    var cardNumber = document.getElementById('cardNumber').value;
    cardNumber = cardNumber.replace(/\s/g, '');
    cardNumber = cardNumber.replace(/-/g, '');
    var cardRGEX = /^4[0-9]{12}(?:[0-9]{3})?$/;
    var cardResult = cardRGEX.test(cardNumber);
    if(cardResult){
        return true;
    } else {
        alert("Invalid card number entered. Only numbers,dashes and spaces are permitted.")
        return false;
    }
}

