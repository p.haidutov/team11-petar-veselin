package a16team11.virtualwallet.service;

import a16team11.virtualwallet.exception.EntityNotFoundException;
import a16team11.virtualwallet.exception.PasswordsDoNotMatchException;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTests {

    @Mock
    private UserRepository mockRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserDetailsManager userDetailsManager;

    @InjectMocks
    private UserServiceImpl mockService;

    @Test(expected = EntityNotFoundException.class)
    public void getUserById_ShouldThrowError_WhenNoSuchUser(){
        //Arrange
        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(null);

        //Act
        mockService.getUserById(5);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserByUsername_ShouldThrowError_WhenNoSuchUser(){
        //Arrange
        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(null);

        //Act
        mockService.getUserByUsername("5");
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserByEmail_ShouldThrowError_WhenNoSuchUser(){
        //Arrange
        when(mockRepository.getUserByEmailAndEnabledTrue(anyString())).thenReturn(null);

        //Act
        mockService.getUserByEmail("5");
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserByPhone_ShouldThrowError_WhenNoSuchUser(){
        //Arrange
        when(mockRepository.getUserByPhoneAndEnabledTrue(anyString())).thenReturn(null);

        //Act
        mockService.getUserByPhone("5");
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByField_ShouldThrowError_WhenNoSuchUser(){
        //Arrange
        when(mockRepository.getUserByPhoneAndEnabledTrue(anyString())).thenReturn(null);
        when(mockRepository.getUserByEmailAndEnabledTrue(anyString())).thenReturn(null);
        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(null);
        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(null);

        //Act
        mockService.getByField(5, "", "", "");
    }

    @Test
    public void findUserByEmailIgnoreCase_ShouldReturnUser_WhenNoErrors(){
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "0888989815", true, false);
        when(mockRepository.findByEmailIgnoreCase(anyString())).thenReturn(user);

        //Act and Assert
        Assert.assertEquals(user, mockService.findByEmailIgnoreCase("pesho"));
    }

    @Test
    public void save_ShouldBeExecutedOnce_WhenNoErrors(){
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "0888989815", true, false);

        //Act
        mockService.save(user);

        //Assert
        verify(mockRepository, times(1)).save(user);
    }

    @Test
    public void changePassword_ShouldBeExecutedOnce_WhenNoErrors(){
        //Arrange
        String oldPassword = "1234";
        String newPassword = "12345";
        String confirmPassword = "12345";
        User user = new User();
        user.setPassword(oldPassword);

        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(passwordEncoder.encode(newPassword)).thenReturn(newPassword);
        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act
        mockService.updatePassword(oldPassword, newPassword, confirmPassword, "pesho");

        //Assert
        verify(userDetailsManager, times(1)).changePassword(oldPassword,newPassword);
    }

    @Test(expected = PasswordsDoNotMatchException.class)
    public void changePassword_ShouldThrowError_WhenOldPasswordIsWrong(){
        //Arrange
        String oldPassword = "1234";
        String newPassword = "12345";
        String confirmPassword = "12345";
        User user = new User();
        user.setPassword(oldPassword);

        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);
        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockService.updatePassword(oldPassword, newPassword, confirmPassword, "pesho");
    }

    @Test(expected = PasswordsDoNotMatchException.class)
    public void changePassword_ShouldThrowError_WhenPassAndConfirmDontMatch(){
        //Arrange
        String oldPassword = "1234";
        String newPassword = "123456";
        String confirmPassword = "12345";
        User user = new User();
        user.setPassword(oldPassword);

        //Act and Assert
        mockService.updatePassword(oldPassword, newPassword, confirmPassword, "pesho");
    }
}
