package a16team11.virtualwallet.service;

import a16team11.virtualwallet.models.Token;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.TokenRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TokenServiceTests {

    @Mock
    private TokenRepository mockRepository;

    @InjectMocks
    private TokenServiceImpl mockService;

    @InjectMocks
    private JavaMailSenderImpl mailSender;

    @Test
    public void findToken_ShouldReturnToken_WhenNoErrors(){
        //Arrange
        Token token = new Token(5, "token");
        when(mockRepository.findByToken(token.getToken())).thenReturn(token);

        //Act and Assert
        Assert.assertEquals(token, mockService.find("token"));
    }

    @Test
    public void save_ShouldBeExecutedOnce_WhenNoErrors(){
        //Arrange
        Token token = new Token(5, "token");

        //Act
        mockService.save(token);

        //Assert
        verify(mockRepository, times(1)).save(token);
    }

    @Test
    public void delete_ShouldBeExecutedOnce_WhenNoErrors(){
        //Arrange
        Token token = new Token(5, "token");

        //Act
        mockService.delete(token);

        //Assert
        verify(mockRepository, times(1)).delete(token);
    }
}
