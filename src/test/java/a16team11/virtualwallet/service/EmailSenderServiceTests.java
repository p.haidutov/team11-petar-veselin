package a16team11.virtualwallet.service;

import a16team11.virtualwallet.models.Token;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.TokenRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EmailSenderServiceTests {

    @Mock
    private TokenRepository mockRepository;

    @InjectMocks
    private EmailSenderServiceImpl mockService;

    @Mock
    private TokenServiceImpl tokenService;

    @Mock
    private JavaMailSender mailSender;

    @Test
    public void sendEmail_ShouldGetProperMessage_WhenNoErrors(){
        //Arrange
        Token token = new Token(5, "token");
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "0888989815", true, false);
        token.setUser(user);

        when(mockRepository.save(any(Token.class))).thenReturn(token);

        //Act
        mockService.sendEmail(user);

        //Act and Assert
        verify(tokenService, times(1)).save(any(Token.class));
        verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
    }
}
