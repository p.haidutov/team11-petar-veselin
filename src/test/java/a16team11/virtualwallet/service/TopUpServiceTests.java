package a16team11.virtualwallet.service;

import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.repository.TopUpRepository;
import a16team11.virtualwallet.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TopUpServiceTests {

    @Mock
    private TopUpRepository topUpRepository;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private TopUpServiceImpl topUpService;

    @Mock
    private UserServiceImpl userService;
    @Mock
    private WalletServiceImpl walletService;

    @Test
    public void getAllByUsername_ShouldReturnAllTopUps_WhenNoErrors(){
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        TopUp topUp = new TopUp(5, BigDecimal.valueOf(250));
        TopUp topUp2 = new TopUp(6, BigDecimal.valueOf(240));
        topUp.setUser(user);
        topUp2.setUser(user);
        List<TopUp> transactions = new ArrayList<>();
        transactions.add(topUp);
        transactions.add(topUp2);
        when(topUpRepository.getAllByUserOrderByDateDesc(user)).thenReturn(transactions);

        //Act and Assert
//        Assert.assertEquals(transactions, topUpService.getAllByUsername(user));
    }

    @Test
    public void getTop5ByUsername_ShouldReturnTop5_WhenNoErrors(){
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        TopUp topUp = new TopUp(5, BigDecimal.valueOf(250));
        TopUp topUp2 = new TopUp(6, BigDecimal.valueOf(240));
        TopUp topUp3 = new TopUp(7, BigDecimal.valueOf(230));
        TopUp topUp4 = new TopUp(8, BigDecimal.valueOf(220));
        TopUp topUp5 = new TopUp(9, BigDecimal.valueOf(210));
        topUp.setUser(user);
        topUp2.setUser(user);
        topUp3.setUser(user);
        topUp4.setUser(user);
        topUp5.setUser(user);
        List<TopUp> transactions = new ArrayList<>();
        transactions.add(topUp);
        transactions.add(topUp2);
        transactions.add(topUp3);
        transactions.add(topUp4);
        transactions.add(topUp5);
        when(topUpRepository.getTop5ByUserOrderByDateDesc(user)).thenReturn(transactions);

        //Act and Assert
        Assert.assertEquals(transactions, topUpService.getTop5ByUsername(user));
    }

    @Test
    public void getAllByUsernameAndDate_ShouldReturnAllTopUps_WhenNoErrors() throws ParseException {
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        TopUp topUp = new TopUp(5, BigDecimal.valueOf(250));
        TopUp topUp2 = new TopUp(6, BigDecimal.valueOf(240));
        topUp.setUser(user);
        topUp2.setUser(user);
        List<TopUp> transactions = new ArrayList<>();
        transactions.add(topUp);
        transactions.add(topUp2);
        String startDate = "01-2020";
        String endDate = "05-2020";
        Date startDateParsed = new SimpleDateFormat("MM-yyyy").parse(startDate);
        Date endDateParsed = new SimpleDateFormat("MM-yyyy").parse(endDate);
        when(topUpRepository.getAllByUserAndDateBetweenOrderByDate(user, startDateParsed, endDateParsed))
                .thenReturn(transactions);

        //Act and Assert
        Assert.assertEquals(transactions, topUpService.getAllByUsernameAndDate(user, startDateParsed, endDateParsed));
    }

    @Test
    public void getAllByUsernameOrUsernameAndDate_ShouldReturnAllTopUps_WhenNoErrors() throws ParseException {
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        User user2 = new User(51, "pesho1", "123453",
                "ppesho@gmail.com", "+359888989811", true, false);
        TopUp topUp = new TopUp(5, BigDecimal.valueOf(250));
        TopUp topUp2 = new TopUp(6, BigDecimal.valueOf(240));
        topUp.setUser(user);
        topUp2.setUser(user);
        List<TopUp> transactions = new ArrayList<>();
        transactions.add(topUp);
        transactions.add(topUp2);
        String startDate = "01-2020";
        String endDate = "05-2020";
        Date startDateParsed = new SimpleDateFormat("MM-yyyy").parse(startDate);
        Date endDateParsed = new SimpleDateFormat("MM-yyyy").parse(endDate);
        when(topUpRepository.getAllByUserOrUserAndDateBetweenOrderByDate(user, user2, startDateParsed, endDateParsed))
                .thenReturn(transactions);

        //Act and Assert
        Assert.assertEquals(transactions, topUpService.getAllByUsernameOrUsernameAndDate(
                user, user2, startDateParsed, endDateParsed));
    }

    @Test
    public void getAllByDate_ShouldReturnAllTopUps_WhenNoErrors() throws ParseException {
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        User user2 = new User(51, "pesho1", "123453",
                "ppesho@gmail.com", "+359888989811", true, false);
        TopUp topUp = new TopUp(5, BigDecimal.valueOf(250));
        TopUp topUp2 = new TopUp(6, BigDecimal.valueOf(240));
        topUp.setUser(user);
        topUp2.setUser(user);
        List<TopUp> transactions = new ArrayList<>();
        transactions.add(topUp);
        transactions.add(topUp2);
        String startDate = "01-2020";
        String endDate = "05-2020";
        Date startDateParsed = new SimpleDateFormat("MM-yyyy").parse(startDate);
        Date endDateParsed = new SimpleDateFormat("MM-yyyy").parse(endDate);
        when(topUpRepository.getAllByDateBetweenOrderByDate(startDateParsed, endDateParsed))
                .thenReturn(transactions);

        //Act and Assert
        Assert.assertEquals(transactions, topUpService.getAllByDate(startDateParsed, endDateParsed));
    }

    @Test
    public void create_ShouldReturnTopUp_WhenRequestIsValid() {
        //Arrange
        TopUp request = new TopUp(1, BigDecimal.valueOf(2000));
        request.setWallet(new Wallet());
        request.setCard(new Card());
        request.setIdempotencyKey("8c4951d6-6301-40c1-8fbb-0d83f1a12a29");
        request.setDescription("description");
        User user = new User(50, "pesho", "123456",
                "pesho@gmail.com", "+359888989815", true, false);
        request.setUser(user);
        when(topUpRepository.getByIdempotencyKey(anyString())).thenReturn(request);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);
        when(userService.getUserByUsername(anyString())).thenReturn(user);

        //Act and Assert
        topUpService.topUp(user.getUsername(), request);
        verify(walletService, times(1)).changeBalance(request.getWallet(), request.getAmount());
    }
}