package a16team11.virtualwallet.integration;

import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.models.WalletType;
import a16team11.virtualwallet.models.dto.UserCreationDto;
import a16team11.virtualwallet.repository.CategoryRepository;
import a16team11.virtualwallet.repository.UserRepository;
import a16team11.virtualwallet.repository.WalletRepository;
import a16team11.virtualwallet.repository.WalletTypeRepository;
import a16team11.virtualwallet.service.common.EmailSenderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static a16team11.virtualwallet.utils.Constants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserIntegrationTests {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository mockRepository;
    @MockBean
    private WalletRepository walletRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private WalletTypeRepository walletTypeRepository;
    @MockBean
    private EmailSenderService emailSenderService;



    @BeforeEach
    public void setUpUser() {
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        UserCreationDto userCreationDto = new UserCreationDto("peshec-pesho",
                "123456", "123456",
                "peshec-pesho@gmail.com", "+359888989815");
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getAll_ShouldReturnUsers_WhenUsersExist() throws Exception {
        //Arrange
        List<User> users = Arrays.asList(
                new User(50, "peshec-pesho", "123456",
                        "peshec-pesho@gmail.com", "+359888989815", true, false),
                new User(51, "peshec-pesho2", "123456",
                        "peshec-pesho2@gmail.com", "+359888989814", true, false)
        );
        Page<User> result = new PageImpl<>(users, PageRequest.of(0, 5), users.size());
        when(mockRepository.findAllByEnabledTrue()).thenReturn(users);

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(2)));

    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getAll_ShouldThrowError_WhenUserListIsEmpty() throws Exception {
        //Arrange
        Page<User> users = new PageImpl<>(new ArrayList<>(), PageRequest.of(0, 5), 0);
//        when(mockRepository.findAllByEnabledTrue(PageRequest.of(0, 5))).thenReturn(users);

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/users"))
                .andExpect(status().isNotFound());
//                .andExpect(jsonPath("$.message", is(EMPTY_USERS_LIST_ERROR)));

    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getByField_ShouldReturnUser_WhenUserExistsById() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(user);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/users/?id=50"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(50)))
                .andExpect(jsonPath("$.username", is("peshec-pesho")));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getByField_ShouldReturnUser_WhenUserExistsByUsername() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/users/?username=any"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(50)))
                .andExpect(jsonPath("$.username", is("peshec-pesho")));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getByField_ShouldReturnUser_WhenUserExistsByEmail() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        when(mockRepository.getUserByEmailAndEnabledTrue(anyString())).thenReturn(user);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/users/?email=any"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(50)))
                .andExpect(jsonPath("$.username", is("peshec-pesho")));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getByField_ShouldReturnUser_WhenUserExistsByPhone() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        when(mockRepository.getUserByPhoneAndEnabledTrue(anyString())).thenReturn(user);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/users/?phone=any"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(50)))
                .andExpect(jsonPath("$.username", is("peshec-pesho")));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void create_ShouldCreateUser_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        UserCreationDto userCreationDto = new UserCreationDto("peshec-pesho",
                "123456", "123456",
                "peshec-pesho@gmail.com", "+359888989815");
        when(mockRepository.save(any(User.class))).thenReturn(user);
        when(mockRepository.existsUserByUsername(anyString())).thenReturn(false);
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(false);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(false);
        doNothing().when(mockRepository).updateAuthorities(anyString());
        doNothing().when(emailSenderService).sendEmail(any(User.class));
        when(walletRepository.save(any(Wallet.class))).thenReturn(new Wallet());
        when(walletTypeRepository.getByType(anyString())).thenReturn(new WalletType());
        when(categoryRepository.save(any(Category.class))).thenReturn(new Category());


        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/users/registration")
                .content(om.writeValueAsString(userCreationDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void create_ShouldThrowError_WhenPasswordsDoNotMatch() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        UserCreationDto userCreationDto = new UserCreationDto("peshec-pesho",
                "123456", "12345",
                "peshec-pesho@gmail.com", "+359888989815");
        when(mockRepository.save(any(User.class))).thenReturn(user);
        when(mockRepository.existsUserByUsername(anyString())).thenReturn(false);
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(false);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(false);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/users/registration")
                .content(om.writeValueAsString(userCreationDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void create_ShouldThrowError_WhenWrongEmailFormat() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        UserCreationDto userCreationDto = new UserCreationDto("peshec-pesho",
                "123456", "123456",
                "peshec-peshogmail.com", "+359888989815");
        when(mockRepository.save(any(User.class))).thenReturn(user);
        when(mockRepository.existsUserByUsername(anyString())).thenReturn(false);
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(false);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(false);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/users/registration")
                .content(om.writeValueAsString(userCreationDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void create_ShouldThrowError_WhenUsernameAlreadyInUse() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        UserCreationDto userCreationDto = new UserCreationDto("peshec-pesho",
                "123456", "123456",
                "peshec-pesho@gmail.com", "+359888989815");
        when(mockRepository.save(any(User.class))).thenReturn(user);
        when(mockRepository.existsUserByUsername(anyString())).thenReturn(true);
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(false);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(false);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/users/registration")
                .content(om.writeValueAsString(userCreationDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(USERNAME_ALREADY_USED_ERROR)));
    }

    @Test
    public void create_ShouldThrowError_WhenEmailAlreadyInUse() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        UserCreationDto userCreationDto = new UserCreationDto("peshec-pesho",
                "123456", "123456",
                "peshec-pesho@gmail.com", "+359888989815");
        when(mockRepository.save(any(User.class))).thenReturn(user);
        when(mockRepository.existsUserByUsername(anyString())).thenReturn(false);
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(true);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(false);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/users/registration")
                .content(om.writeValueAsString(userCreationDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(EMAIL_ALREADY_USED_ERROR)));
    }

    @Test
    public void create_ShouldThrowError_WhenPhoneAlreadyInUse() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        UserCreationDto userCreationDto = new UserCreationDto("peshec-pesho",
                "123456", "123456",
                "peshec-pesho@gmail.com", "+359888989815");
        when(mockRepository.save(any(User.class))).thenReturn(user);
        when(mockRepository.existsUserByUsername(anyString())).thenReturn(false);
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(false);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(true);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/users/registration")
                .content(om.writeValueAsString(userCreationDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(PHONE_ALREADY_USED_ERROR)));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getKey_ShouldReturnKey_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);

        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);
        when(mockRepository.save(any(User.class))).thenReturn(user);

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/users/key"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("pesho")
    public void update_ShouldUpdateUser_WhenNoErrors() throws Exception {
        //Arrange
        User userOld = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989814", true, false);
        User userUpdate = new User(51, "peshec-pesho2", "123456",
                "peshec-pesho2@gmail.com", "+359888989814", true, false);

        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(userOld);
        when(mockRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList(ADMIN));
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(false);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(false);
        when(mockRepository.save(any(User.class))).thenReturn(userUpdate);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/users/10")
                .content(om.writeValueAsString(userUpdate))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser("pesho")
    public void update_ShouldThrowError_WhenEmailAlreadyExists() throws Exception {
        //Arrange
        User userOld = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        User userUpdate = new User(51, "peshec-pesho2", "123456",
                "peshec-pesho2@gmail.com", "+359888989814", true, false);

        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(userOld);
        when(mockRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList(ADMIN));
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(true);
        when(mockRepository.getUserByEmailAndEnabledTrue(anyString())).thenReturn(userUpdate);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(false);
        when(mockRepository.save(any(User.class))).thenReturn(userUpdate);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/users/10")
                .content(om.writeValueAsString(userUpdate))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(EMAIL_ALREADY_USED_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void update_ShouldThrowError_WhenPhoneAlreadyExists() throws Exception {
        //Arrange
        User userOld = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        User userUpdate = new User(51, "peshec-pesho2", "123456",
                "peshec-pesho2@gmail.com", "+359888989814", true, false);

        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(userOld);
        when(mockRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList(ADMIN));
        when(mockRepository.existsUserByEmail(anyString())).thenReturn(false);
        when(mockRepository.existsUserByPhoneAndEnabledTrue(anyString())).thenReturn(true);
        when(mockRepository.getUserByPhoneAndEnabledTrue(anyString())).thenReturn(userUpdate);
        when(mockRepository.save(any(User.class))).thenReturn(userUpdate);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/users/10")
                .content(om.writeValueAsString(userUpdate))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(PHONE_ALREADY_USED_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void update_ShouldThrowError_WhenUserNotAuthorized() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        User user2 = new User(51, "peshec-pesho2", "123456",
                "peshec-pesho2@gmail.com", "+359888989814", true, false);

        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(user);
        when(mockRepository.getUserAuthorities(anyString())).thenReturn(Collections.emptyList());
        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user2);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/users/10")
                .content(om.writeValueAsString(user2))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(USER_NOT_AUTHORIZED_TO_ACCESS_THIS_RESOURCE)));
    }

    @Test
    @WithMockUser("pesho")
    public void delete_ShouldDeleteUser_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(user);
        when(mockRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList(ADMIN));
        when(mockRepository.save(any(User.class))).thenReturn(user);

        //Act and Assert
        mockMvc.perform(delete("http://localhost:8080/api/users/7"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser("mitko")
    public void delete_ShouldThrowError_WhenUserIsNotAuthorized() throws Exception {
        //Arrange
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        User user2 = new User(51, "peshec-pesho2", "123456",
                "peshec-pesho2@gmail.com", "+359888989814", true, false);

        when(mockRepository.getUserByIdAndEnabledTrue(anyInt())).thenReturn(user);
        when(mockRepository.getUserAuthorities(anyString())).thenReturn(Collections.emptyList());
        when(mockRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user2);
        when(mockRepository.save(any(User.class))).thenReturn(user);

        //Act and Assert
        mockMvc.perform(delete("http://localhost:8080/api/users/7"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(USER_NOT_AUTHORIZED_TO_ACCESS_THIS_RESOURCE)));
    }
}
