package a16team11.virtualwallet.integration;

import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.repository.UserRepository;
import a16team11.virtualwallet.repository.WalletRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.*;

import static a16team11.virtualwallet.utils.Constants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class WalletIntegrationTests {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletRepository mockRepository;

    @MockBean
    private UserRepository userRepository;

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getByField_ShouldReturnWallet_WhenWalletExistsById() throws Exception {
        //Arrange
        Wallet wallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        when(mockRepository.getWalletByIdAndEnabledTrue(anyInt())).thenReturn(wallet);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/wallets/?id=50"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(50)))
                .andExpect(jsonPath("$.name", is("Pesho's wallet")));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getByField_ShouldReturnWallet_WhenWalletExistsByName() throws Exception {
        //Arrange
        Wallet wallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        when(mockRepository.getWalletByNameAndEnabledTrue(anyString())).thenReturn(wallet);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/wallets/?name=Pesho's wallet"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(50)))
                .andExpect(jsonPath("$.name", is("Pesho's wallet")));
    }

    @Test
    @WithMockUser("peshec")
    public void getByField_ShouldReturnStatus403_WhenUserHasNoAuthority() throws Exception {
        //Arrange
        User user = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        Wallet wallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        wallet.setUser(user);
        when(mockRepository.getWalletByIdAndEnabledTrue(anyInt())).thenReturn(wallet);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/wallets/?id=50"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(WALLET_DOES_NOT_BELONG_TO_USER_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void getByField_ShouldReturnStatus404_WhenNoSuchWalletById() throws Exception {
        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/wallets/?id=150"))
                .andExpect(status().isNotFound());
//                .andExpect(jsonPath("$.message", is(NO_SUCH_WALLET_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void getByField_ShouldReturnStatus404_WhenNoSuchWalletByName() throws Exception {
        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/wallets/?name=No such Wallet"))
                .andExpect(status().isNotFound());
//                .andExpect(jsonPath("$.message", is(NO_SUCH_WALLET_ERROR)));
    }

    @Test
    @WithMockUser("mitko")
    public void create_ShouldCreateWallet_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        Wallet wallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        wallet.setUser(user);
        when(mockRepository.save(any(Wallet.class))).thenReturn(wallet);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/wallets")
                .content(om.writeValueAsString(wallet))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser("mitko")
    public void create_ShouldThrowException_WhenWalletNameAlreadyExistsForUser() throws Exception {
        //Arrange
        Wallet wallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        when(mockRepository.existsWalletByNameAndUser(anyString(), any())).thenReturn(true);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/wallets")
                .content(om.writeValueAsString(wallet))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(USER_ALREADY_HAS_SUCH_WALLET_ERROR)));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void update_ShouldUpdateWallet_WhenNoErrors() throws Exception {
        //Arrange
        Wallet oldWallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        Wallet updateWallet = new Wallet(50, true, "Pesho's wallet2", BigDecimal.valueOf(150.0));

        when(mockRepository.getWalletByIdAndEnabledTrue(anyInt())).thenReturn(oldWallet);
        when(mockRepository.save(any(Wallet.class))).thenReturn(updateWallet);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/wallets/10")
                .content(om.writeValueAsString(updateWallet))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void delete_ShouldDeleteWallet_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        Wallet wallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        wallet.setUser(user);

        when(mockRepository.getWalletByIdAndEnabledTrue(anyInt())).thenReturn(wallet);
        when(mockRepository.save(any(Wallet.class))).thenReturn(wallet);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(delete("http://localhost:8080/api/wallets/7"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void delete_ShouldReturnStatus403_WhenUserHasNoAuthority() throws Exception {
        //Arrange
        User user = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        Wallet wallet = new Wallet(50, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        wallet.setUser(user);
        when(mockRepository.getWalletByIdAndEnabledTrue(anyInt())).thenReturn(wallet);

        //Act and Assert
        mockMvc.perform(delete("http://localhost:8080/api/wallets/50"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(WALLET_DOES_NOT_BELONG_TO_USER_ERROR)));
    }
}
