package a16team11.virtualwallet.integration;

import a16team11.virtualwallet.models.Category;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.CategoryRepository;
import a16team11.virtualwallet.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CategoryIntegrationTests {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private UserRepository userRepository;

    @Test
    @WithMockUser("vesko")
    public void getAll_ShouldReturnCategories_WhenCategoriesExist() throws Exception {
        //Arrange
        List<Category> categories = Arrays.asList(
                new Category("General", "vesko"),
                new Category("Secondary", "vesko")
        );
        when(categoryRepository.getAllByUsernameAndEnabledTrue("vesko")).thenReturn(categories);

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/categories"))
                .andExpect(status().isOk());
//                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].name", is("General")))
//                .andExpect(jsonPath("$[1].name", is("Secondary")));
    }

    @Test
    @WithMockUser("mitko")
    public void create_ShouldCreateCategory_WhenNoErrors() throws Exception {
        //Arrange
        Category category = new Category("General", "mitko");
        when(categoryRepository.save(category)).thenReturn(category);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/categories")
                .content(om.writeValueAsString(category))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser("pesho")
    public void update_ShouldUpdateCategory_WhenNoErrors() throws Exception {
        //Arrange
        Category category = new Category("General", "pesho");
        category.setId(1);
        Category updatedCategory = new Category("Secondary", "pesho");
        User user = new User(50, "pesho", "123456",
                "pesho@gmail.com", "+359888989815", true, false);
        when(categoryRepository.getByIdAndUsername(1, category.getUsername())).thenReturn(category);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);
        when(categoryRepository.save(any(Category.class))).thenReturn(updatedCategory);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/categories/1")
                .content(om.writeValueAsString(updatedCategory))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser("pesho")
    public void delete_ShouldDeleteCategory_WhenNoErrors() throws Exception {
        //Arrange
        Category category = new Category("General", "pesho");
        category.setId(2);
        Category updatedCategory = new Category("Secondary", "pesho");
        User user = new User(50, "pesho", "123456",
                "pesho@gmail.com", "+359888989815", true, false);
        when(categoryRepository.getByIdAndUsername(2, category.getUsername())).thenReturn(category);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);
        when(categoryRepository.save(any(Category.class))).thenReturn(updatedCategory);

        //Act and Assert
        mockMvc.perform(delete("http://localhost:8080/api/categories/2"))
                .andExpect(status().isNoContent());
    }
}
