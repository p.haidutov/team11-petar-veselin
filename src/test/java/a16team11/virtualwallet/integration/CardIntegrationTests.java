package a16team11.virtualwallet.integration;


import a16team11.virtualwallet.models.Card;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.repository.CardRepository;
import a16team11.virtualwallet.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.*;

import static a16team11.virtualwallet.utils.Constants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CardIntegrationTests {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CardRepository mockRepository;

    @MockBean
    private UserRepository userRepository;

    private static List<Card> testCards;

    @BeforeAll
    public static void setUp() throws Exception {
        String date = "01-2020";
        Date dateParsed = new SimpleDateFormat("MM-yyyy").parse(date);
        testCards = new ArrayList<>();
        Card card = new Card(15, true, "4234-5678-9123-5555",
                 "123", "Pesho Picha");
        Card card2 = new Card(16, true, "4234-5678-9123-1111",
                 "234", "Pesho Picha2");
        testCards.add(card);
        testCards.add(card2);
    }

    @Test
    @WithMockUser("pesho")
    public void getByField_ShouldReturnCard_WhenCardExistsById() throws Exception {
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card card = new Card(15, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        card.setUser(user);
        when(mockRepository.getCardByIdAndEnabledTrue(anyInt())).thenReturn(card);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/cards/?id=15"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(15)))
                .andExpect(jsonPath("$.ownerName", is("Pesho Picha")));
    }

    @Test
    @WithMockUser("pesho")
    public void getByField_ShouldReturnCard_WhenCardExistsByNumber() throws Exception {
        //Arrange
        User user = new User(50, "mitko", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card card = new Card(15, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        card.setUser(user);
        when(mockRepository.getCardByNumberAndEnabledTrue(anyString())).thenReturn(card);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Assert and Act
        mockMvc.perform(get("http://localhost:8080/api/cards/?number=4234-5678-9123-5555"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(15)))
                .andExpect(jsonPath("$.ownerName", is("Pesho Picha")))
                .andExpect(jsonPath("$.number", is("4234-5678-9123-5555")));
    }

    @Test
    @WithMockUser("mitko")
    public void getByField_ShouldReturnStatus403_WhenUserHasNoAuthority() throws Exception {
        //Arrange
        User user = new User(50, "mitko", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card card = new Card(15, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        when(mockRepository.getCardByIdAndEnabledTrue(anyInt())).thenReturn(card);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/cards/?id=15"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(CARD_DOES_NOT_BELONG_TO_USER_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void getByField_ShouldReturnStatus404_WhenNoSuchCardById() throws Exception {
        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/cards/?id=150"))
                .andExpect(status().isNotFound());
//                .andExpect(jsonPath("$.message", is(NO_SUCH_CARD_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void getByField_ShouldReturnStatus404_WhenNoSuchCardByNumber() throws Exception {
        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/cards/?number=0000-0000-0000-0000"))
                .andExpect(status().isNotFound());
//                .andExpect(jsonPath("$.message", is(NO_SUCH_CARD_ERROR)));
    }

    @Test
    @WithMockUser("mitko")
    public void create_ShouldCreateCard_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(50, "mitko", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card card = new Card(33, true, "4234-5678-9123-3333",
                "123", "Pesho Picha");
        card.setUser(user);
        card.setExp_date(new SimpleDateFormat("yyyy-MM").parse("2020-03"));
        when(mockRepository.save(card)).thenReturn(card);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/cards")
                .content(om.writeValueAsString(card))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser("mitko")
    public void create_ShouldThrowException_WhenCardNumberAlreadyExists() throws Exception {
        //Arrange
        User user = new User(50, "mitko", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card card = new Card(15, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        card.setUser(user);
        card.setExp_date(new SimpleDateFormat("yyyy-MM").parse("2020-03"));
        when(mockRepository.existsCardByNumber(anyString())).thenReturn(true);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/cards")
                .content(om.writeValueAsString(card))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(DUPLICATE_CARD_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void update_ShouldUpdateCard_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card oldCard = new Card(15, true, "4234-5678-9123-3232",
                "234", "Pesho Picha");
        oldCard.setUser(user);
        Card updateCard = new Card(16, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");

        when(mockRepository.getCardByIdAndEnabledTrue(anyInt())).thenReturn(oldCard);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);
        when(mockRepository.save(any(Card.class))).thenReturn(updateCard);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/cards/1")
                .content(om.writeValueAsString(updateCard))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser("pesho")
    public void update_ShouldThrowNotFound_WhenNoCardWithThisId() throws Exception {
        //Arrange
        Card updateCard = new Card(16, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        when(mockRepository.getCardByIdAndEnabledTrue(15)).thenReturn(null);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/cards/15")
                .content(om.writeValueAsString(updateCard))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
//                .andExpect(jsonPath("$.message", is(NO_SUCH_CARD_ERROR)));
    }

    @Test
    @WithMockUser("doni")
    public void update_ShouldThrowUnauthorized_WhenUserHasNoAuthority() throws Exception {
        //Arrange
        User user = new User(50, "doni", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card oldCard = new Card(15, true, "4234-5678-9123-3232",
                "234", "Pesho Picha");
        Card updateCard = new Card(16, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        when(mockRepository.getCardByIdAndEnabledTrue(15)).thenReturn(oldCard);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/cards/15")
                .content(om.writeValueAsString(updateCard))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(CARD_DOES_NOT_BELONG_TO_USER_ERROR)));
    }

    @Test
    @WithMockUser(value = "blocked")
    public void update_ShouldThrowUnauthorized_WhenUserIsBlocked() throws Exception {
        //Arrange
        User user = new User(50, "blocked", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card oldCard = new Card(15, true, "4234-5678-9123-3232",
                "234", "Pesho Picha");
        oldCard.setUser(user);
        user.setBlocked(true);
        Card updateCard = new Card(15, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        when(mockRepository.getCardByIdAndEnabledTrue(15)).thenReturn(oldCard);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(put("http://localhost:8080/api/cards/15")
                .content(om.writeValueAsString(updateCard))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(USER_IS_BLOCKED_ERROR)));
    }

    @Test
    @WithMockUser("pesho")
    public void delete_ShouldDeleteCard_WhenNoErrors() throws Exception {
        //Arrange
        User user = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card card = new Card(16, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");
        card.setUser(user);
        when(mockRepository.getCardByIdAndEnabledTrue(anyInt())).thenReturn(card);
        when(mockRepository.save(any(Card.class))).thenReturn(card);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(delete("http://localhost:8080/api/cards/7"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser("mitko")
    public void delete_ShouldReturnStatus403_WhenUserHasNoAuthority() throws Exception {
        //Arrange
        User user = new User(44, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);
        Card card = new Card(16, true, "4234-5678-9123-5555",
                "123", "Pesho Picha");

        when(mockRepository.getCardByIdAndEnabledTrue(anyInt())).thenReturn(card);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);

        //Act and Assert
        mockMvc.perform(delete("http://localhost:8080/api/cards/50"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is(CARD_DOES_NOT_BELONG_TO_USER_ERROR)));
    }
}
