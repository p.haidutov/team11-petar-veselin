package a16team11.virtualwallet.integration;

import a16team11.virtualwallet.models.TopUp;
import a16team11.virtualwallet.models.User;
import a16team11.virtualwallet.models.Wallet;
import a16team11.virtualwallet.models.dto.TopUpDto;
import a16team11.virtualwallet.repository.TopUpRepository;
import a16team11.virtualwallet.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static a16team11.virtualwallet.utils.Constants.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TopUpIntegrationTests {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TopUpRepository mockRepository;

    @MockBean
    private UserRepository userRepository;

    @Test
    @WithMockUser("pesho")
    public void create_ShouldReturnTopUp_WhenRequestIsValid() throws Exception {
        //Arrange
        TopUpDto dto = new TopUpDto(1, 1, 1, BigDecimal.valueOf(2000), "description");
        TopUp request = new TopUp(1, BigDecimal.valueOf(2000));
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("idempotencyKey", "key1");
        dto.setKey("key1");

        when(mockRepository.getByIdempotencyKey(anyString())).thenReturn(request);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);
        when(mockRepository.existsByIdempotencyKey(anyString())).thenReturn(false);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/top-ups")
                .content(om.writeValueAsString(dto))
                .headers(httpHeaders))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser("pesho")
    public void create_ShouldReturnError_WhenIdempotencyKeyExists() throws Exception {
        //Arrange
        TopUpDto dto = new TopUpDto(15, 1, 1, BigDecimal.valueOf(2000), "description");
        TopUp request = new TopUp(15, BigDecimal.valueOf(2000));
        Wallet wallet = new Wallet();
        User user = new User(50, "peshec-pesho", "123456",
                "peshec-pesho@gmail.com", "+359888989815", true, false);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("idempotencyKey", "8c4951d6-6301-40c1-8fbb-0d83f1a12a29");

        when(mockRepository.getByIdempotencyKey(anyString())).thenReturn(request);
        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user);
        when(mockRepository.existsByIdempotencyKey(anyString())).thenReturn(true);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/top-ups")
                .content(om.writeValueAsString(dto))
                .headers(httpHeaders))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(TRANSACTION_ALREADY_PROCESSED_ERROR)));
    }
}
