package a16team11.virtualwallet.integration;

import a16team11.virtualwallet.models.*;
import a16team11.virtualwallet.models.dto.TransactionCreationDto;
import a16team11.virtualwallet.repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static a16team11.virtualwallet.utils.Constants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TransactionIntegrationTests {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionRepository mockRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private WalletRepository walletRepository;

    @MockBean
    private WalletTypeRepository walletTypeRepository;

    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private TopUpRepository topUpRepository;

    @Test
    @WithMockUser("pesho")
    public void create_ShouldReturnTransaction_WhenRequestIsValid() throws Exception {
        //Arrange
        Category category = new Category();
        category.setId(1);
        TransactionCreationDto request = new TransactionCreationDto();
        request.setToUser("mitko");
        request.setFromWalletId(1);
        request.setDescription("description of transaction");
        request.setAmount(BigDecimal.valueOf(2000));
        request.setCategoryId(1);
        Transaction transaction = new Transaction();
        transaction.setId(15);
        transaction.setAmount(BigDecimal.valueOf(2000));
        User user1 = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "0888989815", true, false);
        User user2 = new User(51, "mitko", "123456",
                "peshec-pesho2@gmail.com", "0888989811", true, false);
        WalletType walletType = new WalletType(1, "primary");
        Wallet wallet1 = new Wallet();
        wallet1.setId(1);
        wallet1.setBalance(BigDecimal.valueOf(3000));
        wallet1.setWalletType(walletType);
        wallet1.setUser(user1);
        Wallet wallet2 = new Wallet();
        wallet2.setId(2);
        wallet2.setWalletType(walletType);
        wallet2.setUser(user2);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("idempotencyKey", "key1");

        when(mockRepository.getByIdempotencyKey(anyString())).thenReturn(transaction);
        when(userRepository.getUserByUsernameAndEnabledTrue("pesho")).thenReturn(user1);
        when(userRepository.getUserByUsernameAndEnabledTrue("mitko")).thenReturn(user2);
        when(mockRepository.existsByIdempotencyKey(anyString())).thenReturn(false);
        when(walletTypeRepository.getByType(anyString())).thenReturn(walletType);
        when(walletRepository.getWalletByUserAndWalletType(user2, walletType)).thenReturn(wallet2);
        when(walletRepository.getWalletByIdAndEnabledTrue(1)).thenReturn(wallet1);
        when(walletRepository.getWalletByIdAndEnabledTrue(2)).thenReturn(wallet2);
        when(categoryRepository.getByIdAndEnabledTrue(1)).thenReturn(category);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/transactions/1")
                .content(om.writeValueAsString(request))
                .headers(httpHeaders))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser("pesho")
    public void create_ShouldThrowError_WhenFromWalletHasInsufficientFunds() throws Exception {
        //Arrange
        TransactionCreationDto request = new TransactionCreationDto();
        request.setToUser("pesho");
        request.setFromWalletId(1);
        request.setDescription("description of transaction");
        request.setAmount(BigDecimal.valueOf(2000));
        request.setToUser("mitko");
        Transaction transaction = new Transaction();
        transaction.setId(15);
        transaction.setAmount(BigDecimal.valueOf(2000));
        User user1 = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "0888989815", true, false);
        User user2 = new User(51, "mitko", "123456",
                "peshec-pesho2@gmail.com", "0888989811", true, false);
        Wallet wallet1 = new Wallet(1, true, "walletche", BigDecimal.valueOf(1999));
        wallet1.setUser(user1);
        Wallet wallet2 = new Wallet(2, true, "walletche2", BigDecimal.valueOf(1999));
        wallet2.setUser(user2);
        WalletType walletType = new WalletType();


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("idempotencyKey", "8c4951d6-6301-40c1-8fbb-0d83f1a12a29");

        when(mockRepository.getByIdempotencyKey(anyString())).thenReturn(transaction);
        when(userRepository.getUserByUsernameAndEnabledTrue("pesho")).thenReturn(user1);
        when(userRepository.getUserByUsernameAndEnabledTrue("mitko")).thenReturn(user2);
        when(mockRepository.existsByIdempotencyKey(anyString())).thenReturn(false);
        when(walletRepository.save(wallet1)).thenReturn(wallet1);
        when(walletRepository.save(wallet2)).thenReturn(wallet2);

        when(walletRepository.getWalletByIdAndEnabledTrue(1)).thenReturn(wallet1);
        when(walletRepository.getWalletByIdAndEnabledTrue(2)).thenReturn(wallet2);
        when(walletTypeRepository.getByType(anyString())).thenReturn(walletType);
        when(walletRepository.getWalletByUserAndWalletType(user2, walletType)).thenReturn(wallet2);


        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/transactions/1")
                .content(om.writeValueAsString(request))
                .headers(httpHeaders))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message").value(INSUFFICIENT_FUNDS_IN_WALLET_ERROR));
    }

    @Test
    @WithMockUser("pesho")
    public void create_ShouldThrowError_WhenIdempotencyKey() throws Exception {
        //Arrange
        TransactionCreationDto request = new TransactionCreationDto();
        request.setToUser("mitko");
        request.setFromWalletId(2);
        request.setDescription("description of transaction");
        request.setAmount(BigDecimal.valueOf(2000));
        Transaction transaction = new Transaction();
        transaction.setId(15);
        transaction.setAmount(BigDecimal.valueOf(2000));
        User user1 = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "0888989815", true, false);
        User user2 = new User(51, "mitko", "123456",
                "peshec-pesho2@gmail.com", "0888989811", true, false);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("idempotencyKey", "8c4951d6-6301-40c1-8fbb-0d83f1a12a29");

        when(mockRepository.getByIdempotencyKey(anyString())).thenReturn(transaction);
        when(userRepository.getUserByUsernameAndEnabledTrue("pesho")).thenReturn(user1);
        when(userRepository.getUserByUsernameAndEnabledTrue("mitko")).thenReturn(user2);
        when(walletRepository.getWalletByIdAndEnabledTrue(anyInt())).thenReturn(new Wallet());
        when(mockRepository.existsByIdempotencyKey(anyString())).thenReturn(true);
        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/transactions/1")
                .content(om.writeValueAsString(request))
                .headers(httpHeaders))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message").value(TRANSACTION_ALREADY_PROCESSED_ERROR));
    }

    @Test
    @WithMockUser("pesho")
    public void create_ShouldThrowError_WhenWalletsDoNotBelongToUsers() throws Exception {
        //Arrange
        TransactionCreationDto request = new TransactionCreationDto();
        request.setToUser("mitko");
        request.setFromWalletId(1);
        request.setDescription("description of transaction");
        request.setAmount(BigDecimal.valueOf(2000));
        request.setCategoryId(1);
        Transaction transaction = new Transaction();
        transaction.setId(1);
        transaction.setAmount(BigDecimal.valueOf(2000));
        User user1 = new User(50, "pesho", "123456",
                "peshec-pesho@gmail.com", "0888989815", true, false);
        User user2 = new User(51, "mitko", "123456",
                "peshec-pesho2@gmail.com", "0888989811", true, false);
        Wallet wallet1 = new Wallet();
        wallet1.setId(1);
        wallet1.setBalance(BigDecimal.valueOf(1999));
        wallet1.setWalletType(new WalletType(1, "primary"));
        wallet1.setUser(user1);
        Wallet wallet2 = new Wallet();
        wallet2.setId(2);
        wallet2.setWalletType(new WalletType(2, "primary"));
        wallet2.setUser(user1);
        WalletType walletType = new WalletType();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        httpHeaders.add("idempotencyKey", "8c4951d6-6301-40c1-8fbb-0d83f1a12a29");

        when(mockRepository.getByIdempotencyKey(anyString())).thenReturn(transaction);
        when(userRepository.getUserByUsernameAndEnabledTrue("pesho")).thenReturn(user1);
        when(userRepository.getUserByUsernameAndEnabledTrue("mitko")).thenReturn(user2);
        when(mockRepository.existsByIdempotencyKey(anyString())).thenReturn(false);
        when(walletRepository.getWalletByIdAndEnabledTrue(1)).thenReturn(wallet1);
        when(walletRepository.getWalletByIdAndEnabledTrue(2)).thenReturn(wallet2);
        when(walletTypeRepository.getByType(anyString())).thenReturn(walletType);
        when(walletRepository.getWalletByUserAndWalletType(any(User.class), any(WalletType.class)))
                .thenReturn(wallet2);

        //Act and Assert
        mockMvc.perform(post("http://localhost:8080/api/transactions/1")
                .content(om.writeValueAsString(request))
                .headers(httpHeaders))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message").value(WALLET_DOES_NOT_BELONG_TO_USER_ERROR));
    }

    @Test
    @WithMockUser("pesho")
    public void getTop5_ShouldReturnTop5_WhenNoErrors() throws Exception {
        //Arrange
        User user1 = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        User user2 = new User(2, "mitko", "123456",
                "mitec-mitko@gmail.com", "+359888202020", true, false);
        Wallet fromWallet = new Wallet(1, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        Wallet toWallet = new Wallet(2, true, "Mitko's wallet", BigDecimal.valueOf(200.0));
        Category category = new Category("General", "pesho");
        Card card = new Card(1, true, "4234 5678 9123 3232",
                "234", "Pesho Picha");

        List<Transaction> list = Arrays.asList(
                new Transaction(1, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(20), "key", "desc", "BGN", category, new Date()),
                new Transaction(2, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(4), "key2", "desc3", "BGN", category, new Date())
        );

        List<TopUp> topUps = Arrays.asList(
                new TopUp(1, user1, card, fromWallet, BigDecimal.valueOf(100),
                        "desc", "BGN", "key", new Date()),
                new TopUp(2, user1, card, fromWallet, BigDecimal.valueOf(200),
                        "desc", "BGN", "key2", new Date())
        );

        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user1);
        when(mockRepository.getTop5ByFromUserOrToUserOrderByDateDesc(anyString(), anyString())).thenReturn(list);
        when(topUpRepository.getTop5ByUserOrderByDateDesc(any(User.class))).thenReturn(topUps);

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/transactions/top5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(4)));
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getAll_ShouldReturnAll_WhenNoErrors() throws Exception {
        //Arrange
        User user1 = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        User user2 = new User(2, "mitko", "123456",
                "mitec-mitko@gmail.com", "+359888202020", true, false);
        Wallet fromWallet = new Wallet(1, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        Wallet toWallet = new Wallet(2, true, "Mitko's wallet", BigDecimal.valueOf(200.0));
        Category category = new Category("General", "pesho");
        Card card = new Card(1, true, "4234 5678 9123 3232",
                "234", "Pesho Picha");

        List<Transaction> list = Arrays.asList(
                new Transaction(1, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(20), "key", "desc", "BGN", category, new Date()),
                new Transaction(2, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(4), "key2", "desc3", "BGN", category, new Date())
        );

        List<TopUp> topUps = Arrays.asList(
                new TopUp(1, user1, card, fromWallet, BigDecimal.valueOf(100),
                        "desc", "BGN", "key", new Date()),
                new TopUp(2, user1, card, fromWallet, BigDecimal.valueOf(200),
                        "desc", "BGN", "key2", new Date())
        );

        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user1);
        when(mockRepository.getTransactionByFromUserContainingAndToUserContainingAndDateBetween(anyString(),
                anyString(), any(Date.class), any(Date.class))).thenReturn(list);
        when(topUpRepository.getAllByDateBetweenOrderByDate(any(Date.class), any(Date.class)))
                .thenReturn(topUps);
        when(userRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList("ROLE_ADMIN"));

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/transactions/all"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getAll_ShouldReturnAll_WhenSortByAsc() throws Exception {
        //Arrange
        User user1 = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        User user2 = new User(2, "mitko", "123456",
                "mitec-mitko@gmail.com", "+359888202020", true, false);
        Wallet fromWallet = new Wallet(1, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        Wallet toWallet = new Wallet(2, true, "Mitko's wallet", BigDecimal.valueOf(200.0));
        Category category = new Category("General", "pesho");
        Card card = new Card(1, true, "4234 5678 9123 3232",
                "234", "Pesho Picha");

        List<Transaction> list = Arrays.asList(
                new Transaction(1, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(20), "key", "desc", "BGN", category, new Date()),
                new Transaction(2, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(4), "key2", "desc3", "BGN", category, new Date())
        );

        List<TopUp> topUps = Arrays.asList(
                new TopUp(1, user1, card, fromWallet, BigDecimal.valueOf(100),
                        "desc", "BGN", "key", new Date()),
                new TopUp(2, user1, card, fromWallet, BigDecimal.valueOf(200),
                        "desc", "BGN", "key2", new Date())
        );

        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user1);
        when(mockRepository.getTransactionByFromUserContainingAndToUserContainingAndDateBetween(anyString(),
                anyString(), any(Date.class), any(Date.class))).thenReturn(list);
        when(topUpRepository.getAllByDateBetweenOrderByDate(any(Date.class), any(Date.class)))
                .thenReturn(topUps);
        when(userRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList("ROLE_ADMIN"));

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/transactions/all?sortBy=DateAsc"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getAll_ShouldReturnAll_WhenSortByAmountDesc() throws Exception {
        //Arrange
        User user1 = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        User user2 = new User(2, "mitko", "123456",
                "mitec-mitko@gmail.com", "+359888202020", true, false);
        Wallet fromWallet = new Wallet(1, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        Wallet toWallet = new Wallet(2, true, "Mitko's wallet", BigDecimal.valueOf(200.0));
        Category category = new Category("General", "pesho");
        Card card = new Card(1, true, "4234 5678 9123 3232",
                "234", "Pesho Picha");

        List<Transaction> list = Arrays.asList(
                new Transaction(1, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(20), "key", "desc", "BGN", category, new Date()),
                new Transaction(2, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(4), "key2", "desc3", "BGN", category, new Date())
        );

        List<TopUp> topUps = Arrays.asList(
                new TopUp(1, user1, card, fromWallet, BigDecimal.valueOf(100),
                        "desc", "BGN", "key", new Date()),
                new TopUp(2, user1, card, fromWallet, BigDecimal.valueOf(200),
                        "desc", "BGN", "key2", new Date())
        );

        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user1);
        when(mockRepository.getTransactionByFromUserContainingAndToUserContainingAndDateBetween(anyString(),
                anyString(), any(Date.class), any(Date.class))).thenReturn(list);
        when(topUpRepository.getAllByDateBetweenOrderByDate(any(Date.class), any(Date.class)))
                .thenReturn(topUps);
        when(userRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList("ROLE_ADMIN"));

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/transactions/all?sortBy=AmountDesc"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "pesho", roles = {"USER", "ADMIN"})
    public void getAll_ShouldReturnAll_WhenSortByAmountAsc() throws Exception {
        //Arrange
        User user1 = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        User user2 = new User(2, "mitko", "123456",
                "mitec-mitko@gmail.com", "+359888202020", true, false);
        Wallet fromWallet = new Wallet(1, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        Wallet toWallet = new Wallet(2, true, "Mitko's wallet", BigDecimal.valueOf(200.0));
        Category category = new Category("General", "pesho");
        Card card = new Card(1, true, "4234 5678 9123 3232",
                "234", "Pesho Picha");

        List<Transaction> list = Arrays.asList(
                new Transaction(1, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(20), "key", "desc", "BGN", category, new Date()),
                new Transaction(2, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(4), "key2", "desc3", "BGN", category, new Date())
        );

        List<TopUp> topUps = Arrays.asList(
                new TopUp(1, user1, card, fromWallet, BigDecimal.valueOf(100),
                        "desc", "BGN", "key", new Date()),
                new TopUp(2, user1, card, fromWallet, BigDecimal.valueOf(200),
                        "desc", "BGN", "key2", new Date())
        );

        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user1);
        when(mockRepository.getTransactionByFromUserContainingAndToUserContainingAndDateBetween(anyString(),
                anyString(), any(Date.class), any(Date.class))).thenReturn(list);
        when(topUpRepository.getAllByDateBetweenOrderByDate(any(Date.class), any(Date.class)))
                .thenReturn(topUps);
        when(userRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList("ROLE_ADMIN"));

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/transactions/all?sortBy=AmountAsc"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("peshec")
    public void getByUsername_ShouldReturnTransactions_WhenSortByAmountAsc() throws Exception {
        //Arrange
        User user1 = new User(1, "pesho", "123456",
                "peshec-pesho@gmail.com", "+359888202030", true, false);
        User user2 = new User(2, "mitko", "123456",
                "mitec-mitko@gmail.com", "+359888202020", true, false);
        Wallet fromWallet = new Wallet(1, true, "Pesho's wallet", BigDecimal.valueOf(150.0));
        Wallet toWallet = new Wallet(2, true, "Mitko's wallet", BigDecimal.valueOf(200.0));
        Category category = new Category("General", "pesho");
        Card card = new Card(1, true, "4234 5678 9123 3232",
                "234", "Pesho Picha");

        List<Transaction> list = Arrays.asList(
                new Transaction(1, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(20), "key", "desc", "BGN", category, new Date()),
                new Transaction(2, "pesho", "mitko", fromWallet, toWallet,
                        BigDecimal.valueOf(4), "key2", "desc3", "BGN", category, new Date())
        );

        List<TopUp> topUps = Arrays.asList(
                new TopUp(1, user1, card, fromWallet, BigDecimal.valueOf(100),
                        "desc", "BGN", "key", new Date()),
                new TopUp(2, user1, card, fromWallet, BigDecimal.valueOf(200),
                        "desc", "BGN", "key2", new Date())
        );

        when(userRepository.getUserByUsernameAndEnabledTrue(anyString())).thenReturn(user1);
        when(mockRepository.getTransactionByFromUserOrToUserAndToUserContainingAndDateBetween(anyString(), anyString(),
                anyString(), any(Date.class), any(Date.class))).thenReturn(list);
        when(topUpRepository.getAllByUserAndDateBetweenOrderByDate(any(User.class), any(Date.class), any(Date.class)))
                .thenReturn(topUps);
        when(userRepository.getUserAuthorities(anyString())).thenReturn(Collections.singletonList("ROLE_ADMIN"));

        //Act and Assert
        mockMvc.perform(get("http://localhost:8080/api/transactions?sortBy=AmountAsc"))
                .andExpect(status().isOk());
    }
}
